<?php

//常量定义
define("APP_PATH", "E:/www/library_system/");
define("SITE_URL", "http://library.com/");
define("ADMIN_TEM_DIR", APP_PATH."templates/admin/");
define("MEMBER_TEM_DIR", APP_PATH."templates/member/");
define("CSS_PATH", SITE_URL."public/css/");
define("JS_PATH", SITE_URL."public/js/");
define("IMG_PATH", SITE_URL."public/img/");
define("ERROR_PAGE", SITE_URL."errorpage.php");
define("EACHPAGE", 10); //每页显示的条数


//数据库配置信息
$dbConfig = array(
	'dsn'  => 'mysql:dbname=db_library;host=localhost',
	'user' => 'root',
	'password' => 'fdj123',
	'charset' => 'utf8',
	);

//smarty 配置信息
$smarty_template_dir	='./templates/';
$smarty_compile_dir		='./templates_c/';
$smarty_config_dir		='./configs/';
$smarty_cache_dir		='./cache/';
$smarty_caching			=false;
$smarty_delimiter		=explode("|","{|}");

?>