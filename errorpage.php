<?php 
include_once("./global.php");

if ($_GET) {
	$errMsg = $_GET['msg'];

	$assignVar = array('errMsg'=>$errMsg,
					   'url'=>SITE_URL);
	$smarty->assign($assignVar);
}

$smarty->display("errorpage.html");

?>