<?php
	include_once('./global.php');
	
	if (isset($_SESSION)) {
		session_destroy();
		header("Location: ".SITE_URL."login.php");
	}
?>