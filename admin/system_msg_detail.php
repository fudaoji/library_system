<?php 
	/**
	 * @desc 具体图书分类信息页
	 */
	
	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'system_msg_detail.css');
	$jsArr  = array('system_msg_detail.js');

	if ($_GET) {
		$id = intval($_GET['id']);
		$option = array('table' => $systemMsgTable,
						'where' => array("id"=>$id),
						);
		$msgDetail = isExist($option, $db);
		if (!$msgDetail) {
			header("Location: ".ERROR_PAGE."?msg=该系统消息不存在");exit;
		}
	}

	$assignVar = array( "yesorno"=>$yesorno,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"msgDetail"=>$msgDetail,
						);
	$smarty->assign($assignVar);
	$smarty->display(ADMIN_TEM_DIR."system_msg_detail.html");
	
?>
