<?php
	/**
	 * @desc 删除图书分类
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$id = intval($_POST['id']);
		$option = array('id'=>$id);
		if (delObj($bookTypeTable, $option, $db)) {
			$base->ajax(true, "删除成功");
		}
		$base->ajax(false, "删除失败，请重新操作");
	}

?>