<?php 
	/**
	 * @desc 具体图书具体信息页
	 */
	
	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'book_detail.css');
	$jsArr  = array('book_detail.js');

	if ($_GET) {
		$id = intval($_GET['id']);
		$option = array('table' => $bookDetailTable,
						'where' => array("id"=>$id),
						);
		$bookDetail = isExist($option, $db);
		if (!$bookDetail) {
			header("Location: ".ERROR_PAGE."?msg=该书不存在");exit;
		}
		//所有分类
		$typeList = getList(array('table'=>$bookTypeTable), $db);
	}

	$assignVar = array( "isShow"=>$isShow,
						"isLend"=>$isLend,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"bookDetail"=>$bookDetail,
						"typeList"=>$typeList,
						);
	$smarty->assign($assignVar);
	$smarty->display(ADMIN_TEM_DIR."book_detail.html");
	
?>
