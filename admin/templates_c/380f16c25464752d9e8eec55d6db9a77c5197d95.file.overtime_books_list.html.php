<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 16:05:18
         compiled from "E:\www\library_system\templates\admin\overtime_books_list.html" */ ?>
<?php /*%%SmartyHeaderCode:4337525ce8710f4009-72440286%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '380f16c25464752d9e8eec55d6db9a77c5197d95' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\overtime_books_list.html',
      1 => 1381823928,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4337525ce8710f4009-72440286',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525ce87116e244_73378938',
  'variables' => 
  array (
    'bookList' => 0,
    'book' => 0,
    'userInfo' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525ce87116e244_73378938')) {function content_525ce87116e244_73378938($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"超期书籍列表"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">书籍管理</span> >> 
			<span class="softFont">
				超期书籍
			</span>
		</div>
		<div id="main_body">
			<?php if ($_smarty_tpl->tpl_vars['bookList']->value) {?>
			<center>
				<form id="listForm">
					<table id="bookList" cellpadding="5" cellspacing="0">
						<tr>
							<th>图书号</th>
							<th>图书名</th>
							<th>借出时间</th>
							<th>截止时间</th>
							<th>借书者</th>
							<th class="wider">操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['book'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['book']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bookList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['book']->key => $_smarty_tpl->tpl_vars['book']->value) {
$_smarty_tpl->tpl_vars['book']->_loop = true;
?>
						<tr>
							<td><?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['book']->value['title'];?>
</td>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['book']->value['begin_time'],"%Y-%m-%d");?>
</td>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['book']->value['end_time'],"%Y-%m-%d");?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['book']->value['username'];?>
</td>
							<td>
								<span class="sixinBtn" name="<?php echo $_smarty_tpl->tpl_vars['book']->value['username'];?>
">
									私信提醒
								</span>
							</td>
						</tr>
						<?php } ?>
					</table>
					<input type="hidden" id="from" value="<?php echo $_smarty_tpl->tpl_vars['userInfo']->value['userName'];?>
" />
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<div class="noContent">暂无超期书籍</div>
			<?php }?>

			<!--私信编辑框-->
			<div class="popBox">
				<center>
				<form>
					<table cellpaddin="0" cellspacing="10">
						<thead class="thead">
							<td colspan="2">发送私信</td>
						</thead>
						<tr>
							<th><label for="sixin">内容:</label></th>
							<td>
								<textarea id="sixin"></textarea>
							</td>
						</tr>
					</table>
				</form>
				<div>
					<span class="submitBtn">提交</span>
					<span class="cancelBtn">取消</span>
				</div>
				</center>
			</div>

		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
