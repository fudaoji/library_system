<?php /* Smarty version Smarty-3.1.15, created on 2013-10-14 10:09:09
         compiled from "E:\www\library_system\templates\admin\books_list.html" */ ?>
<?php /*%%SmartyHeaderCode:271195258fe38ea7df3-70186990%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81ceac350f08b246b18f7a2f188563b661f766ba' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\books_list.html',
      1 => 1381716527,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '271195258fe38ea7df3-70186990',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5258fe39059c87_01677527',
  'variables' => 
  array (
    'typeDetail' => 0,
    'bookList' => 0,
    'site_url' => 0,
    'book' => 0,
    'isLend' => 0,
    'k' => 0,
    'v' => 0,
    'isShow' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258fe39059c87_01677527')) {function content_5258fe39059c87_01677527($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"图书分类列表"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">书籍管理</span> >> 
			<span class="softFont">
				<?php if ($_smarty_tpl->tpl_vars['typeDetail']->value) {?><?php echo $_smarty_tpl->tpl_vars['typeDetail']->value['typename'];?>
类所有书籍
				<?php } else { ?>图书列表
				<?php }?>
			</span>
		</div>
		<div id="main_body">
			<div class="searchBox">
				<input type="text" id="searchKey" name="searchKey" placeholder="图书号或书名" />
				<span class="searchBtn">搜索</span>
			</div>
			<div><button class="addItemBtn">添加此分类书籍</button></div>
			<?php if ($_smarty_tpl->tpl_vars['bookList']->value) {?>
			<center>
				<form id="listForm">
					<table id="typeList" cellpadding="5" cellspacing="0">
						<tr>
							<th>图书号</th>
							<th>图书名</th>
							<th>作者</th>
							<th>出版时间</th>
							<th>是否借出</th>
							<th>上(下)架</th>
							<th class="wider">操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['book'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['book']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bookList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['book']->key => $_smarty_tpl->tpl_vars['book']->value) {
$_smarty_tpl->tpl_vars['book']->_loop = true;
?>
						<tr>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/book_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['book']->value['id'];?>
">
									<?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>

								</a>
							</td>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/book_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['book']->value['id'];?>
">
									<?php echo $_smarty_tpl->tpl_vars['book']->value['title'];?>

								</a>
							</td>
							<td><?php echo $_smarty_tpl->tpl_vars['book']->value['author'];?>
</td>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['book']->value['pubtime'],"%Y-%m-%d");?>
</td>
							<td>
								<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['isLend']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['book']->value['is_lend']) {?><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
<?php }?>
								<?php } ?>
							</td>
							<td>
								<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['isShow']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
								<?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['book']->value['is_show']) {?><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
<?php }?>
								<?php } ?>
							</td>
							<td>
								&nbsp;
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/book_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['book']->value['id'];?>
" class="editBtn">
									编辑
								</a>&nbsp;&nbsp;
								<span class="delBtn" name="<?php echo $_smarty_tpl->tpl_vars['book']->value['id'];?>
">删除</span>
							</td>
						</tr>
						<?php } ?>
					</table>
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<div class="noContent">暂时没有藏书</div>
			<?php }?>

			<!--添加书籍框-->
			<div class="popBox" style="">
				<center>
				<form id="editForm">
					<table cellpaddin="0" cellspacing="10">
						<thead class="thead">
							<td colspan="2">新书上架</td>
						</thead>
						<tr>
							<th><label for="bid">图&nbsp;书&nbsp;号:</label></th>
							<td>
								<input type="text" name="bid" id="bid" />
							</td>
						</tr>
						<tr>
							<th><label for="title">图&nbsp;书&nbsp;名:</label></th>
							<td>
								<input type="text" name="title" id="title" />
							</td>
						</tr>
						<tr>
							<th>
								<label for="author">作&nbsp;&nbsp;&nbsp;&nbsp;者:</label>
							</th>
							<td>
								<input type="text" name="author" id="author" />
							</td>
						</tr>
						<tr>
							<th>
								<label for="description">简&nbsp;&nbsp;&nbsp;&nbsp;介:</label>
							</th>
							<td>
								<textarea name="description" id="description" cols="20" rows="5" ></textarea>
							</td>
						</tr>
						<tr>
							<th><label for="pubtime">出版时间:</label></th>
							<td>
								<input onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" id="pubtime" name="pubtime" class="Wdate" type="text" />
							</td>
						</tr>
						<tr>
							<th><label for="isShow">是否上架:</label></th>
							<td>
								<select id="isShow">
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['isShow']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</option>
									<?php } ?>
								</select>
							</td>
						</tr>
					</table>
					<input type="hidden" id="typeID" value="<?php echo $_smarty_tpl->tpl_vars['typeDetail']->value['typeid'];?>
"  />
				</form>
				<div>
					<span class="submitBtn">提交</span>
					<span class="cancelBtn">取消</span>
				</div>
				</center>
			</div>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
