<?php /* Smarty version Smarty-3.1.15, created on 2013-10-16 16:54:34
         compiled from "E:\www\library_system\templates\admin\upload_data.html" */ ?>
<?php /*%%SmartyHeaderCode:15598525e49e5e1a430-69661539%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1be0da4a0be6c4176596ca39070de4903818d298' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\upload_data.html',
      1 => 1381913650,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15598525e49e5e1a430-69661539',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525e49e5e57230_63039120',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525e49e5e57230_63039120')) {function content_525e49e5e57230_63039120($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"导入数据"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">用户管理</span> >> 
			<span class="softFont">导入数据</span>
		</div>
		<div id="main_body">
			<center>
			<form class="uploadForm" method="post" action="/admin/upload_data.php" enctype="multipart/form-data">
			    <table>   
			      	<tr>    
				        <td>
				        	<input type="hidden" name="MAX_FILE_SIZE" value="2000000">
				        	文件： 
				        </td>   
				        <td>    
					        <input name="infoFile" type="file" value="浏览" >              
					        <input type="submit" value="上传" id="submit">   
				        </td>   
			      	</tr>   
			    </table>   
			</form>
			<div class="attention" style="font-style:italic; color:gray;">
		        <h4 class="attention_title">上传文件注意事项</h4>
		        <div class="attention_body">
		            <ul>
		                <li>1、文件最大不超过 <strong>2 MB</strong>.</li>
		                <li>2、文件后缀名只能为.xls或.xlsx</li>
		                <li>3、excel文件格式如下(例子)：<br>
							<table cellspacing='0' cellpadding="5" border='1'>
								<tr>
									<td>username</td>
									<td>realname</td>
									<td>sex</td>
								</tr>
								<tr>
									<td>101</td>
									<td>小明</td>
									<td>1</td>
								</tr>
							</table>
		                </li>	
		            </ul>
		        </div>
	    	</div> 
	    	</center>  
		</div>
	</div>
<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
