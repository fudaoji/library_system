<?php /* Smarty version Smarty-3.1.15, created on 2013-10-16 15:40:04
         compiled from "E:\www\library_system\templates\admin\members_list.html" */ ?>
<?php /*%%SmartyHeaderCode:160605258c282b1e268-66521719%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '120b41f5df4fac14883a88cd7b96821d69ebcaeb' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\members_list.html',
      1 => 1381909200,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '160605258c282b1e268-66521719',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5258c282b982a3_90754376',
  'variables' => 
  array (
    'memberList' => 0,
    'site_url' => 0,
    'member' => 0,
    'sex' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258c282b982a3_90754376')) {function content_5258c282b982a3_90754376($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"用户列表"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">用户管理</span> >> 
			<span class="softFont">用户列表</span>
		</div>
		<div id="main_body">
			<button class="outputBtn">导出数据</button>
			<button class="uploadBtn">导入数据</button>
			<div class="searchBox">
				<input type="text" id="searchKey" name="searchKey" placeholder="输入账号或真实姓名" />
				<span class="searchBtn">搜索</span>
			</div>
			<?php if ($_smarty_tpl->tpl_vars['memberList']->value) {?>
			<center>
				<form id="listForm">
					<table id="memberList" cellpadding="5" cellspacing="0">
						<tr>
							<th>账号</th>
							<th>真实姓名</th>
							<th>性别</th>
							<th class="wider">操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['member'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['member']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['memberList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['member']->key => $_smarty_tpl->tpl_vars['member']->value) {
$_smarty_tpl->tpl_vars['member']->_loop = true;
?>
						<tr>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/member_detail.php?uid=<?php echo $_smarty_tpl->tpl_vars['member']->value['uid'];?>
" class="editBtn">
									<?php echo $_smarty_tpl->tpl_vars['member']->value['username'];?>

								</a>
							</td>
							<td><?php echo $_smarty_tpl->tpl_vars['member']->value['realname'];?>
 </td>
							<td><?php echo $_smarty_tpl->tpl_vars['sex']->value[$_smarty_tpl->tpl_vars['member']->value['sex']];?>
</td>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/member_detail.php?uid=<?php echo $_smarty_tpl->tpl_vars['member']->value['uid'];?>
" class="editBtn">
									修改信息
								</a>
								<span class="delBtn" name="<?php echo $_smarty_tpl->tpl_vars['member']->value['uid'];?>
">删除</span>
							</td>
						</tr>
						<?php } ?>
					</table>
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<div>暂时没有用户</div>
			<?php }?>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
