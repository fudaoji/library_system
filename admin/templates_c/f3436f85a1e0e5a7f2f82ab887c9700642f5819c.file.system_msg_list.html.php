<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 13:23:37
         compiled from "E:\www\library_system\templates\admin\system_msg_list.html" */ ?>
<?php /*%%SmartyHeaderCode:14126525cb5fbe80056-72029248%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3436f85a1e0e5a7f2f82ab887c9700642f5819c' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\system_msg_list.html',
      1 => 1381809349,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14126525cb5fbe80056-72029248',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525cb5fbefa187_87660938',
  'variables' => 
  array (
    'msgList' => 0,
    'site_url' => 0,
    'msg' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525cb5fbefa187_87660938')) {function content_525cb5fbefa187_87660938($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"系统消息列表"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">书籍管理</span> >> 
			<span class="softFont">系统消息</span>
		</div>
		<div id="main_body">
			<div><button class="addItemBtn">添加系统消息</button></div>
			<?php if ($_smarty_tpl->tpl_vars['msgList']->value) {?>
			<center>
				<form id="listForm">
					<table id="msgList" cellpadding="5" cellspacing="0">
						<tr>
							<th>消息标题</th>
							<th>发布日期</th>
							<th>显示状态</th>
							<th class="wider">操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['msg'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['msg']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['msgList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['msg']->key => $_smarty_tpl->tpl_vars['msg']->value) {
$_smarty_tpl->tpl_vars['msg']->_loop = true;
?>
						<tr>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/system_msg_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['msg']->value['id'];?>
">
									<?php echo $_smarty_tpl->tpl_vars['msg']->value['title'];?>

								</a>
							</td>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['msg']->value['pubtime'],"%Y-%m-%d");?>
</td>
							<td>
								<?php if ($_smarty_tpl->tpl_vars['msg']->value['is_show']) {?>显示
								<?php } else { ?>不显示
								<?php }?>
							</td>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/system_msg_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['msg']->value['id'];?>
" class="editBtn">
									编辑
								</a>
							</td>
						</tr>
						<?php } ?>
					</table>
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<div class="noContent">暂时没有系统消息</div>
			<?php }?>

			<!--添加消息-->
			<div class="popBox" style="">
				<center>
				<form>
					<table cellpaddin="0" cellspacing="10">
						<thead class="thead">
							<td colspan="2">添加系统消息</td>
						</thead>
						<tr>
							<th><label for="title">标题:</label></th>
							<td>
								<input type="text" name="title" id="title" />
							</td>
						</tr>
						<tr>
							<th><label for="content">内容:</label></th>
							<td>
								<textarea id="content"></textarea>
							</td>
						</tr>
						<tr>
							<th><label for="isShow">是否发布:</label></th>
							<td>
								<select id="isShow">
									<option value="1">发布</option>
									<option value="0">不发布</option>
								</select>
							</td>
						</tr>
					</table>
				</form>
				<div>
					<span class="submitBtn">提交</span>
					<span class="cancelBtn">取消</span>
				</div>
				</center>
			</div>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
