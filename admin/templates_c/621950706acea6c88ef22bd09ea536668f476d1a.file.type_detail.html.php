<?php /* Smarty version Smarty-3.1.15, created on 2013-10-12 13:35:06
         compiled from "E:\www\library_system\templates\admin\type_detail.html" */ ?>
<?php /*%%SmartyHeaderCode:105805258df8a962724-04160349%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '621950706acea6c88ef22bd09ea536668f476d1a' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\type_detail.html',
      1 => 1381554273,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '105805258df8a962724-04160349',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'typeDetail' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5258df8a99f4b5_85729214',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258df8a99f4b5_85729214')) {function content_5258df8a99f4b5_85729214($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"编辑用户"), 0);?>

	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">用户管理</span> >> 
			<span class="softFont">编辑图书分类信息</span>
		</div>
		<div id="main_body">
			<center>
				<form id="editForm" name="editForm">
					<div id="formTitle">
						修改<em><?php echo $_smarty_tpl->tpl_vars['typeDetail']->value['typename'];?>
</em>的信息
					</div>
					<table cellpadding="10" cellspacing="10">
						<tr>
							<th>
								<label for="typeID">类别号:</label>
							</th>
							<td>
								<input type="text" name="typeID" id="typeID" value="<?php echo $_smarty_tpl->tpl_vars['typeDetail']->value['typeid'];?>
" />
							</td>
						</tr>
						<tr>
							<th><label for="typeName">类别名:</label></th>
							<td>
								<input type="text" name="typeName" id="typeName" value="<?php echo $_smarty_tpl->tpl_vars['typeDetail']->value['typename'];?>
" />
							</td>
						</tr>
					</table>
					<input type="hidden" id="id" value="<?php echo $_smarty_tpl->tpl_vars['typeDetail']->value['id'];?>
" />
					<div id="saveBtn">保存</div>
				</form>
			</center>
		</div>
	</div>
<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
