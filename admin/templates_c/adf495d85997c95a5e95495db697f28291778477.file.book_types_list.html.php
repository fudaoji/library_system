<?php /* Smarty version Smarty-3.1.15, created on 2013-10-12 16:57:41
         compiled from "E:\www\library_system\templates\admin\book_types_list.html" */ ?>
<?php /*%%SmartyHeaderCode:164105258b83eaf7827-02756865%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'adf495d85997c95a5e95495db697f28291778477' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\book_types_list.html',
      1 => 1381568256,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164105258b83eaf7827-02756865',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5258b83eb71825_44943955',
  'variables' => 
  array (
    'typeList' => 0,
    'site_url' => 0,
    'type' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258b83eb71825_44943955')) {function content_5258b83eb71825_44943955($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"图书分类列表"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">书籍管理</span> >> 
			<span class="softFont">图书分类列表</span>
		</div>
		<div id="main_body">
			<div class="searchBox">
				<input type="text" id="searchKey" name="searchKey" placeholder="类型号或类型名" />
				<span class="searchBtn">搜索</span>
			</div>
			<div><button class="addItemBtn">添加分类</button></div>
			<?php if ($_smarty_tpl->tpl_vars['typeList']->value) {?>
			<center>
				<form id="listForm">
					<table id="typeList" cellpadding="5" cellspacing="0">
						<tr>
							<th>类型号</th>
							<th>类别名</th>
							<th>库存书籍(册)</th>
							<th class="wider">操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['typeList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
?>
						<tr>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/books_list.php?tid=<?php echo $_smarty_tpl->tpl_vars['type']->value['typeid'];?>
">
									<?php echo $_smarty_tpl->tpl_vars['type']->value['typeid'];?>

								</a>
							</td>
							<td>
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/books_list.php?tid=<?php echo $_smarty_tpl->tpl_vars['type']->value['typeid'];?>
">
									<?php echo $_smarty_tpl->tpl_vars['type']->value['typename'];?>

								</a>
							</td>
							<td><?php echo $_smarty_tpl->tpl_vars['type']->value['booknums'];?>
</td>
							<td>
								&nbsp;
								<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/type_detail.php?id=<?php echo $_smarty_tpl->tpl_vars['type']->value['id'];?>
" class="editBtn">
									编辑
								</a>&nbsp;&nbsp;
								<span class="delBtn" name="<?php echo $_smarty_tpl->tpl_vars['type']->value['id'];?>
">删除</span>
							</td>
						</tr>
						<?php } ?>
					</table>
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<div class="noContent">暂时没有分类</div>
			<?php }?>

			<!--添加分类框-->
			<div class="popBox" style="">
				<center>
				<form>
					<table cellpaddin="0" cellspacing="10">
						<thead class="thead">
							<td colspan="2">添加新分类</td>
						</thead>
						<tr>
							<th><label for="typeID">类型号:</label></th>
							<td>
								<input type="text" name="typeID" id="typeID" />
							</td>
						</tr>
						<tr>
							<th><label for="typeName">类型名:</label></th>
							<td>
								<input type="text" name="typeName" id="typeName" />
							</td>
						</tr>
					</table>
				</form>
				<div>
					<span class="submitBtn">提交</span>
					<span class="cancelBtn">取消</span>
				</div>
				</center>
			</div>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
