<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 13:41:41
         compiled from "E:\www\library_system\templates\admin\system_msg_detail.html" */ ?>
<?php /*%%SmartyHeaderCode:32008525cd4e888f5f8-44488038%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '38d2979d9909dc431a38ef8d4d82eb960758e74b' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\system_msg_detail.html',
      1 => 1381815697,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32008525cd4e888f5f8-44488038',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525cd4e89466c3_64539511',
  'variables' => 
  array (
    'msgDetail' => 0,
    'yesorno' => 0,
    'k' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525cd4e89466c3_64539511')) {function content_525cd4e89466c3_64539511($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"系统消息编辑页面"), 0);?>

	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">系统操作</span> >> 
			<span class="softFont">编辑系统消息</span>
		</div>
		<div id="main_body">
			<center>
				<form id="editForm" name="editForm">
					<div id="formTitle">
						修改<em><?php echo $_smarty_tpl->tpl_vars['msgDetail']->value['title'];?>
</em>系统消息
					</div>
					<table cellpadding="10" cellspacing="10">
						<tr>
							<th>
								<label for="title">消息名称:</label>
							</th>
							<td>
								<input type="text" name="title" id="title" value="<?php echo $_smarty_tpl->tpl_vars['msgDetail']->value['title'];?>
" />
							</td>
						</tr>
						<tr>
							<th><label for="content">消息内容:</label></th>
							<td>
								<textarea id="content" /><?php echo $_smarty_tpl->tpl_vars['msgDetail']->value['content'];?>
</textarea>
							</td>
						</tr>
						<tr>
							<th><label for="isShow">是否显示</label></th>
							<td>
								<select id="isShow">
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['yesorno']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['msgDetail']->value['is_show']) {?> selected <?php }?>>
										<?php echo $_smarty_tpl->tpl_vars['v']->value;?>

									</option>
									<?php } ?>
								</select>
							</td>
						</tr>
					</table>
					<input type="hidden" id="id" value="<?php echo $_smarty_tpl->tpl_vars['msgDetail']->value['id'];?>
" />
					<div id="saveBtn">保存</div>
				</form>
			</center>
		</div>
	</div>
<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
