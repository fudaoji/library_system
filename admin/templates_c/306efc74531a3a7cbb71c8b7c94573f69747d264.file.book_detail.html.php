<?php /* Smarty version Smarty-3.1.15, created on 2013-10-14 09:55:31
         compiled from "E:\www\library_system\templates\admin\book_detail.html" */ ?>
<?php /*%%SmartyHeaderCode:2898152592abf848cb5-28828583%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '306efc74531a3a7cbb71c8b7c94573f69747d264' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\book_detail.html',
      1 => 1381715728,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2898152592abf848cb5-28828583',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_52592abf8c2c79_04623938',
  'variables' => 
  array (
    'bookDetail' => 0,
    'typeList' => 0,
    'v' => 0,
    'isShow' => 0,
    'k' => 0,
    'isLend' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52592abf8c2c79_04623938')) {function content_52592abf8c2c79_04623938($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"图书详情"), 0);?>

	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">用户管理</span> >> 
			<span class="softFont">
				<?php if ($_smarty_tpl->tpl_vars['bookDetail']->value['title']) {?>编辑<?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['title'];?>
信息
				<?php } else { ?>编辑图书信息
				<?php }?>
			</span>
		</div>
		<div id="main_body">
			<center>
				<form id="editForm" name="editForm">
					<div id="formTitle">
						<em><?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['title'];?>
</em>的信息
					</div>
					<table cellpadding="5" cellspacing="5">
						<tr>
							<th>
								<label for="bid">图&nbsp;书&nbsp;号:</label>
							</th>
							<td>
								<input type="text" name="bid" id="bid" value="<?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['bid'];?>
" />
							</td>
						</tr>
						<tr>
							<th>
								<label for="title">书&nbsp;&nbsp;&nbsp;&nbsp;名:</label>
							</th>
							<td>
								<input type="text" name="title" id="title" value="<?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['title'];?>
" />
							</td>
						</tr>
						<tr>
							<th>
								<label for="author">作&nbsp;&nbsp;&nbsp;&nbsp;者:</label>
							</th>
							<td>
								<input type="text" name="author" id="author" value="<?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['author'];?>
" />
							</td>
						</tr>
						<tr>
							<th>
								<label for="description">简&nbsp;&nbsp;&nbsp;&nbsp;介:</label>
							</th>
							<td>
								<textarea name="description" id="description" cols="20" rows="5" ><?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['description'];?>
</textarea>
							</td>
						</tr>
						<tr>
							<th>
								<label for="tid">分&nbsp;&nbsp;&nbsp;&nbsp;类:</label>
							</th>
							<td>
								<select name="tid" id="tid" >
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['typeList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['typeid'];?>
" <?php if ($_smarty_tpl->tpl_vars['v']->value['typeid']==$_smarty_tpl->tpl_vars['bookDetail']->value['tid']) {?> selected <?php }?>>
										<?php echo $_smarty_tpl->tpl_vars['v']->value['typename'];?>

									</option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<th><label for="pubtime">出版时间:</label></th>
							<td>
								<input type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})" name="pubtime" id="pubtime" value="<?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['bookDetail']->value['pubtime'],'%Y-%m-%d');?>
" />
							</td>
						</tr>
						<tr>
							<th><label for="isShow">是否上架:</label></th>
							<td>
								<select id="isShow">
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['isShow']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['bookDetail']->value['is_show']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<th><label for="isLend">出借情况:</label></th>
							<td>
								<select id="isLend">
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['isLend']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['bookDetail']->value['is_lend']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value;?>
</option>
									<?php } ?>
								</select>
							</td>
						</tr>
					</table>
					<input type="hidden" id="id" value="<?php echo $_smarty_tpl->tpl_vars['bookDetail']->value['id'];?>
" />
					<div id="saveBtn">保存</div>
				</form>
			</center>
		</div>
	</div>
<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
