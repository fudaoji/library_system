<?php /* Smarty version Smarty-3.1.15, created on 2013-10-12 11:33:43
         compiled from "E:\www\library_system\templates\admin\member_detail.html" */ ?>
<?php /*%%SmartyHeaderCode:275175258c317ce9f72-50504655%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '899a025461d522711deaca27995b611e85aa96d1' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\admin\\member_detail.html',
      1 => 1381479263,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '275175258c317ce9f72-50504655',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'memberDetail' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5258c317da0fa3_39402549',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258c317da0fa3_39402549')) {function content_5258c317da0fa3_39402549($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"编辑用户"), 0);?>

	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">用户管理</span> >> 
			<span class="softFont">修改用户</span>
		</div>
		<div id="main_body">
			<center>
				<form id="editForm" name="editForm">
					<div id="formTitle">
						修改<em><?php echo $_smarty_tpl->tpl_vars['memberDetail']->value['username'];?>
</em>的信息
					</div>
					<table cellpadding="10" cellspacing="10">
						<tr>
							<th>
								<label for="userName">账&nbsp;&nbsp;&nbsp;&nbsp;号:</label>
							</th>
							<td>
								<input type="text" name="userName" id="userName" value="<?php echo $_smarty_tpl->tpl_vars['memberDetail']->value['username'];?>
" />
							</td>
						</tr>
						<tr>
							<th><label for="realName">真实姓名:</label></th>
							<td>
								<input type="text" name="realName" id="realName" value="<?php echo $_smarty_tpl->tpl_vars['memberDetail']->value['realname'];?>
" />
							</td>
						</tr>
						<tr>
							<th><label for="sex">性&nbsp;&nbsp;&nbsp;&nbsp;别:</label></th>
							<td>
								<select name="sex" id="sex">
									<option value="1" <?php if ($_smarty_tpl->tpl_vars['memberDetail']->value['sex']==1) {?> selected <?php }?>>男</option>
									<option value="2" <?php if ($_smarty_tpl->tpl_vars['memberDetail']->value['sex']==2) {?> selected <?php }?>>女</option>
								</select>
							</td>
						</tr>
					</table>
					<input type="hidden" id="uid" value="<?php echo $_smarty_tpl->tpl_vars['memberDetail']->value['uid'];?>
" />
					<div id="saveBtn">保存</div>
				</form>
			</center>
		</div>
	</div>
<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
