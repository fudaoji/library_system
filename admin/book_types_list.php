<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'book_types_list.css');
	$jsArr = array('book_types_list.js');
	$show_pageBox = true;

	$typeNum = $db->getNums(array('table'=>$bookTypeTable,'key'=>'id'));
	$typeList = getList(array('table'=>$bookTypeTable, 'limit'=>'0,'.EACHPAGE), $db);

	//获取搜索结果
	if (isset($_GET['key']) && $_GET['key']) {
		$option = array("table"=>$bookTypeTable, "key"=>$_GET['key']);
		$typeList = searchBookType($option, $db);
		$show_pageBox = false;
	}

	//翻页
	$totalPage = ceil($typeNum/EACHPAGE);
	$currentPage = 1;
	if (isset($_GET['page']) && intval($_GET['page'])>=1) {
		$currentPage = $_GET['page'];
		$option = array("table"=>$bookTypeTable,"limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE);
		$typeList = getList($option, $db);
	}

	$url = SITE_URL."admin/book_types_list.php?page=";
	$pageBox = pageBox($totalPage,$currentPage, $url);
	$assignVar = array( "show_pageBox"=>$show_pageBox,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"typeList"=>$typeList,
						"pageBox"=>$pageBox,
						);
	$smarty->assign($assignVar);

	$smarty->display(ADMIN_TEM_DIR."book_types_list.html");
?>
