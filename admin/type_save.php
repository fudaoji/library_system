<?php
	/**
	 * @desc 保存图书分类信息操作
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$id      = intval($_POST['id']);
		$typeName = trim($_POST['typeName']);
		$typeID = trim($_POST['typeID']);

		$option = array('table'=>$bookTypeTable,'where'=>array('id'=>$id));
		$data = array('typeid'=>$typeID, 'typename'=>$typeName);

		if (saveInfo($option, $data, $db)) {
			$base->ajax(true, "保存分类信息成功");
		}
		$base->ajax(false, "保存分类信息失败，请重新操作");
	}

?>