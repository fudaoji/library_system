<?php
	/**
	 * @desc 保存图书分类信息操作
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$title   = trim($_POST['title']);
		$content = trim($_POST['content']);
		$isShow  = intval($_POST['isShow']);
		$pubtime = time();

		$data = array("title"=>$title, 
					  "content"=>$content, 
					  "pubtime"=>$pubtime, 
					  "is_show"=>$isShow);

		if ($result=$db->insert($systemMsgTable, $data)) 
			$base->ajax(true, '添加系统消息成功');
		$base->ajax(false, "添加系统信息失败，请重新操作!");
	}

?>