<?php
	/**
	 * @desc 修改密码操作
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {

		$userName   = trim($_POST['userName']);
		$newPwd     = trim($_POST['newPwd']);
		$confirmPwd = trim($_POST['confirmPwd']);

		if($newPwd != $confirmPwd)
			$base->ajax(false, "两次输入的密码不一致!");
		
		//插入新密码
		$salt   = $db->salt();
		$newPwd = $db->password($salt, $newPwd);
		$table = 'users';
		$where = " where `username`='$userName'";
		$filter = array('table'=>$table, 'where'=>$where);
		$data = array('salt'=>$salt, 'password'=>$newPwd);
		if(!$db->update($filter, $data))
			$base->ajax(false, "密码修改失败，请重新操作");
		$base->ajax(true, "修改".$_POST['userName']."的密码成功!");
	}


	$cssArr = array('common.css', 'resetpwd.css');
	$jsArr = array('reset_member_pwd.js');

	$assignVar = array(
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						);

	$smarty->assign($assignVar);
	$smarty->display(ADMIN_TEM_DIR."reset_member_pwd.html");
?>


