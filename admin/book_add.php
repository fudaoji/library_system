<?php 
	/**
	 * @desc 添加用户页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {

		$bid    = trim($_POST['bid']);
		$tid    = trim($_POST['tid']);
		$title  = trim($_POST['title']);
		$author = trim($_POST['author']);
		$description = trim($_POST['description']);
		$pubtime = trim($_POST['pubtime']);
		$pubtime = strtotime($pubtime);
		$isShow = intval($_POST['isShow']);

		$option = array('table' => $bookDetailTable,
						'where' => array("bid"=>$bid),
						);

		$result = isExist($option, $db);

		if ($result)
			$base->ajax(false, '该图书已存在');

		$data = array("bid"=>$bid, 
					  "tid"=>$tid,
					  "title"=>$title,
					  "author"=>$author,
					  "description"=>$description,
					  "pubtime"=>$pubtime,
					  "is_show"=>$isShow,
					  );

		//插入数据
		if (!$result=$db->insert($bookDetailTable, $data)) 
			$base->ajax(false, '添加书籍失败');

		//更新该类别图书的数量（增加）
		$option = array('table' => $bookTypeTable,
						'where' => array("typeid"=>$tid),
						);

		$typeDetail = isExist($option, $db);

		$data = array('booknums'=>$typeDetail['booknums']+1);
		if (!saveInfo($option, $data, $db)) {
			$base->ajax(false, '添加书籍失败');
		}

		$base->ajax(true, "添加".$title."书籍成功");
	}
	
?>
