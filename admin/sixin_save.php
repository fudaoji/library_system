<?php
	/**
	 * @desc 保存私信操作
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$from = trim($_POST['from']);
		$to = trim($_POST['to']);
		$sixin = trim($_POST['sixin']);
		$sendtime = time();
	
		$data = array('sender'=>$from, 'receiver'=>$to, 'sixin'=>$sixin, 'sendtime'=>$sendtime);

		if ($result=$db->insert($sixinTable, $data))
			$base->ajax(true,"发送成功");
		
		$base->ajax(false, "发送失败");
	}

?>