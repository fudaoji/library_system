<?php 
	/**
	 * @desc 添加用户页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$id     = trim($_POST['id']);
		$bid    = trim($_POST['bid']);
		$tid    = trim($_POST['tid']);
		$title  = trim($_POST['title']);
		$author = trim($_POST['author']);
		$description = trim($_POST['description']);
		$pubtime = trim($_POST['pubtime']);
		$pubtime = strtotime($pubtime);
		$isShow = intval($_POST['isShow']);
		$isLend = $_POST['isLend'] ? intval($_POST['isLend']) : 0;



		$data = array("bid"=>$bid, 
					  "tid"=>$tid,
					  "title"=>$title,
					  "author"=>$author,
					  "description"=>$description,
					  "pubtime"=>$pubtime,
					  "is_show"=>$isShow,
					  "is_lend"=>$isLend,
					  );

		$option = array('table'=>$bookDetailTable,'where'=>array('id'=>$id));
	
		if (saveInfo($option, $data, $db)) {
			$base->ajax(true, "保存".$title."信息成功");
		}

		$base->ajax(false, "保存".$title."信息失败，请重新操作");
	}
	
?>
