<?php
	/**
	 * @desc 管理员操作公共模块
	 */

	/**
	 * @desc判断对象是否存在
	 */

	function isExist($option, $db){
		$where = array();
		foreach ($option["where"] as $key => $value) {
			if(is_string($value))
				$where[] = "`$key`='$value'";
			else
				$where[] = "`$key`=$value";
		}
		$where = join(' and ', $where);
		$where = " ".$where." ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		if ($obj=$db->getOne($filter)) 
			return $obj;
		return 0;
	}

	/**
	 * @desc获取所有用户信息
	 */
	function getAllMembers($table,$db){
		$memberList = array();
		$where = " `mid`=2 ";
		$filter = array('table'=>$table, 'where'=>$where);
		$result=$db->getAll($filter);
		foreach ($result as $value) {
			$memberList[$value['username']] = $value;
		}
		return $memberList;
	}

	/**
	 * @desc搜索信息
	 */
	function searchMember($option,$db){
		$where = " username='".$option['key']."' ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		if($result=$db->getAll($filter));
		else{
			$where = " realname like '%".$option['key']."%' ";
			$filter = array('table'=>$option['table'], 'where'=>$where);
			$result=$db->getAll($filter);
		}
		return array_values($result);
	}


	/**
	 * @desc删除指定对象
	 */
	function delObj($table, $option, $db){
		foreach ($option as $key => $value) {
			if(is_string($value)) $where = " `$key`='$value' ";
			else $where = " `$key`=$value ";
		}
		$filter = array('table'=>$table, 'where'=>$where);
		return $db->delete($filter);
	}


	/**
	 * @desc更改信息
	 */

	function saveInfo($option, $data, $db){
		$where = array();
		foreach ($option["where"] as $key => $value) {
			if(is_string($value))
				$where[] = "`$key`='$value'";
			else
				$where[] = "`$key`=$value";
		}
		$where = join(' and ', $where);
		$where = " where ".$where." ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		return $db->update($filter, $data);
	}

	/**
	 * @desc搜索图书类别信息
	 */
	function searchBookType($option,$db){
		$where = " typeid='".$option['key']."' ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		if($result=$db->getAll($filter));
		else{
			$where = " typename like '%".$option['key']."%' ";
			$filter = array('table'=>$option['table'], 'where'=>$where);
			$result=$db->getAll($filter);
		}
		return array_values($result);
	}

	/**
	 * @desc搜索图书
	 */
	function searchBooks($option,$db){
		$where = " bid='".$option['key']."' ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		if($result=$db->getAll($filter));
		else{
			$where = " title like '%".$option['key']."%' ";
			$filter = array('table'=>$option['table'], 'where'=>$where);
			$result=$db->getAll($filter);
		}
		return array_values($result);
	}

	/**
	 * @desc 合并两个表的信息
	 */
	function getDetailList($option, $list, $db){
		foreach ($list as &$value) {
			$filter = array('table'=>$option['table'], 'where'=>$option['key']."='".$value[$option['key']]."'");
			$row = $db->getOne($filter);
			$value = array_merge($value, $row);
		} 
		return $list;
	}

?>














