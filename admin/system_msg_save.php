<?php
	/**
	 * @desc 保存系统消息
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$id      = intval($_POST['id']);
		$title   = trim($_POST['title']);
		$content = trim($_POST['content']);
		$isShow  = intval($_POST['isShow']);

		$option = array('table'=>$systemMsgTable,'where'=>array('id'=>$id));
		$data = array('title'=>$title, 'content'=>$content, 'is_show'=>$isShow);

		if (saveInfo($option, $data, $db)) {
			$base->ajax(true, "保存系统消息成功");
		}
		$base->ajax(false, "保存系统消息失败，请重新操作");
	}

?>