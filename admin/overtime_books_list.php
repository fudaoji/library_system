<?php 
	/**
	 * @desc 超期书籍列表页
	 */

		//公共部分
		include_once('./admin_global.php');
	
		$userInfo = $base->auth();

		$cssArr = array('common.css', 'overtime_books_list.css');
		$jsArr = array('overtime_books_list.js');
		$show_pageBox = true;
		$date = time();

		$filter = array('table'=>$memberBookTable,
						'key'=>'id',
						'where'=>"end_time<'".$date."'",
						);
		$bookNum = $db->getNums($filter);
		$reserveList = getList(array('table'=>$memberBookTable,'where'=>"end_time<'".$date."'", 'limit'=>'0,'.EACHPAGE), $db);

		$mergeKey = array('key'=>'bid', 'table'=>$bookDetailTable);
		$bookList = getDetailList($mergeKey, $reserveList, $db);

		//print_r($bookList);exit;

		//翻页
		$totalPage = ceil($bookNum/EACHPAGE);
		$currentPage = 1;
		if (isset($_GET['page']) && intval($_GET['page'])>=1) {
			$currentPage = $_GET['page'];
			$option = array("table"=>$memberBookTable,'where'=>"end_time<'".$date."'","limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE);
			$reserveList = getList($option, $db);
			$bookList = getDetailList($mergeKey, $reserveList, $db);
		}

		$url = SITE_URL."admin/overtime_books_list.php?page=";
		$pageBox = pageBox($totalPage,$currentPage, $url);
		$assignVar = array( "show_pageBox"=>$show_pageBox,
							"userInfo"=>$userInfo, 
							"cssArr"=>$cssArr, 
							"jsArr"=>$jsArr,
							"bookList"=>$bookList,
							"pageBox"=>$pageBox,
							);
		$smarty->assign($assignVar);

		$smarty->display(ADMIN_TEM_DIR."overtime_books_list.html");
?>
