<?php
	/**
	 * @desc 删除用户操作
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$uid = intval($_POST['uid']);
		$option = array('uid'=>$uid);
		if (delObj($usersTable, $option, $db)) {
			$base->ajax(true, "删除成功");
		}
		$base->ajax(false, "删除失败，请重新操作");
	}

?>