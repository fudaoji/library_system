<?php 
	/**
	 * @desc 具体图书分类信息页
	 */
	
	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'type_detail.css');
	$jsArr  = array('type_detail.js');

	if ($_GET) {
		$id = intval($_GET['id']);
		$option = array('table' => $bookTypeTable,
						'where' => array("id"=>$id),
						);
		$typeDetail = isExist($option, $db);
		if (!$typeDetail) {
			header("Location: ".ERROR_PAGE."?msg=该分类不存在");exit;
		}
	}

	$assignVar = array(
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"typeDetail"=>$typeDetail,
						);
	$smarty->assign($assignVar);
	$smarty->display(ADMIN_TEM_DIR."type_detail.html");
	
?>
