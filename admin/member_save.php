<?php
	/**
	 * @desc 保存用户信息操作
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$uid      = intval($_POST['uid']);
		$userName = trim($_POST['userName']);
		$realName = trim($_POST['realName']);
		$sex      = intval($_POST['sex']);

		$option = array('table'=>$usersTable,'where'=>array('uid'=>$uid));
		$data = array('username'=>$userName, 'realname'=>$realName, 'sex'=>$sex);
		if (saveInfo($option, $data, $db)) {
			$base->ajax(true, "保存用户信息成功");
		}
		$base->ajax(false, "保存用户信息失败，请重新操作");
	}

?>