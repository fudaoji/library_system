<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'system_msg_list.css');
	$jsArr = array('system_msg_list.js');
	$show_pageBox = true;

	$msgNum = $db->getNums(array('table'=>$systemMsgTable,'key'=>'id'));
	$msgList = getList(array('table'=>$systemMsgTable, 'limit'=>'0,'.EACHPAGE), $db);


	//翻页
	$totalPage = ceil($msgNum/EACHPAGE);
	$currentPage = 1;
	if (isset($_GET['page']) && intval($_GET['page'])>=1) {
		$currentPage = $_GET['page'];
		$option = array("table"=>$systemMsgTable,"limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE);
		$msgList = getList($option, $db);
	}

	$url = SITE_URL."admin/system_msg_list.php?page=";
	$pageBox = pageBox($totalPage,$currentPage, $url);
	$assignVar = array( "show_pageBox"=>$show_pageBox,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"msgList"=>$msgList,
						"pageBox"=>$pageBox,
						);
	$smarty->assign($assignVar);

	$smarty->display(ADMIN_TEM_DIR."system_msg_list.html");
?>
