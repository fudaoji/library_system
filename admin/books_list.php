<?php 
	/**
	 * @desc 同一分类的所有书籍列表页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if (isset($_GET['tid']) && $_GET['tid']) {

		$cssArr = array('common.css', 'books_list.css');
		$jsArr = array('books_list.js');
		$show_pageBox = true;

		//该类图书的信息
		$option = array('table' => $bookTypeTable,
						'where' => array("typeid"=>$_GET['tid']),
						);
		$typeDetail = isExist($option, $db);

		$filter = array('table'=>$bookDetailTable,
						'key'=>'id',
						'where'=>"tid='".$typeDetail['typeid']."'",
						);
		$bookNum = $db->getNums($filter);
		$bookList = getList(array('table'=>$bookDetailTable,'where'=>"tid='".$typeDetail['typeid']."'", 'limit'=>'0,'.EACHPAGE), $db);

		//获取搜索结果
		if (isset($_GET['key']) && $_GET['key']) {
			$option = array("table"=>$bookDetailTable, "key"=>$_GET['key']);
			$bookList = searchBooks($option, $db);
			$show_pageBox = false;
		}

		//翻页
		$totalPage = ceil($bookNum/EACHPAGE);
		$currentPage = 1;
		if (isset($_GET['page']) && intval($_GET['page'])>=1) {
			$currentPage = $_GET['page'];
			$option = array("table"=>$bookDetailTable,'where'=>"tid='".$typeDetail['typeid']."'","limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE);
			$bookList = getList($option, $db);
		}

		$url = SITE_URL."admin/books_list.php?tid=".$typeDetail['typeid']."&page=";
		$pageBox = pageBox($totalPage,$currentPage, $url);
		$assignVar = array( "show_pageBox"=>$show_pageBox,
							"typeDetail"=>$typeDetail,
							"userInfo"=>$userInfo, 
							"cssArr"=>$cssArr, 
							"jsArr"=>$jsArr,
							"bookList"=>$bookList,
							"pageBox"=>$pageBox,
							"isShow"=>$isShow,
							"isLend"=>$isLend,
							);
		$smarty->assign($assignVar);

		$smarty->display(ADMIN_TEM_DIR."books_list.html");
	}else{
		header("Location: ".ERROR_PAGE."?msg=参数错误");exit;
	}
?>
