<?php 
	/**
	 * @desc 添加用户页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
	
		$typeID   = trim($_POST['typeID']);
		$typeName = trim($_POST['typeName']);

		$option = array('table' => $bookTypeTable,
						'where' => array("typeid"=>$typeID),
						);

		$result = isExist($option, $db);

		if ($result)
			$base->ajax(false, '该类型号已被占用');

		$data = array("typeid"=>$typeID, "typename"=>$typeName);
		if (!$result=$db->insert($bookTypeTable, $data)) 
			$base->ajax(false, '添加分类失败');

		$base->ajax(true, '添加分类成功');
	}
	
?>
