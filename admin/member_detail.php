<?php 
	/**
	 * @desc 具体用户信息页
	 */
	
	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'member_detail.css');
	$jsArr  = array('member_detail.js');

	if ($_GET) {
		$uid = intval($_GET['uid']);
		$option = array('table' => $usersTable,
						'where' => array("uid"=>$uid),
						);
		$memberDetail = isExist($option, $db);
		if (!$memberDetail) {
			header("Location: ".ERROR_PAGE."?msg=该用户不存在");exit;
		}
	}

	$assignVar = array(
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"sex"=>$sex,
						"memberDetail"=>$memberDetail,
						);
	$smarty->assign($assignVar);
	$smarty->display(ADMIN_TEM_DIR."member_detail.html");
	
?>
