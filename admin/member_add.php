<?php 
	/**
	 * @desc 添加用户页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'member_add.css');
	$jsArr  = array('member_add.js');

	if ($_POST) {
	
		$userName = trim($_POST['userName']);
		$mid      = intval($_POST['mid']);
		
		$salt = $db->salt();
		$passWord = $db->password($salt, $userName);
	
		$data = array('mid'=>$mid, "userName"=>$userName, "salt"=>$salt, "password"=>$passWord);
		$option = array('table'=>$usersTable,
						'where'=>array("username"=>$userName),
						);
		$result = isExist($option, $db);
		if ($result)
			$base->ajax(false, '该用户已存在');

		if (!$result=$db->insert($usersTable, $data)) 
			$base->ajax(false, '添加用户失败');

		$base->ajax(true, '添加用户成功');
	}

	$assignVar = array("cssArr"=>$cssArr, "jsArr"=>$jsArr, "userInfo"=>$userInfo);
	$smarty->assign($assignVar);

	$smarty->display(ADMIN_TEM_DIR."member_add.html");
	
?>
