<?php
	/**
	 * @desc 图书下架
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {
		$id = intval($_POST['id']);
		$tid = trim($_POST['tid']);
		$option = array('id'=>$id);

		if (!delObj($bookDetailTable, $option, $db)) {
			$base->ajax(false, "删除失败，请重新操作");
		}

		//更新该类别图书的数量（减1）
		$option = array('table' => $bookTypeTable,
						'where' => array("typeid"=>$tid),
						);
		$typeDetail = isExist($option, $db);
		$data = array('booknums'=>$typeDetail['booknums']-1);
		if (!saveInfo($option, $data, $db)) {
			$base->ajax(false, '添加书籍失败');
		}

		$base->ajax(true, "删除成功");
	}

?>