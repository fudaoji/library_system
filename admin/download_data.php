<?php 

	/**
	 * @desc 导出数据保存为excel格式
	 */

	include_once ('../configs/config.php');
	include_once ('../library/mysql.php');
	include_once ('../library/page.php');
	include_once ('../library/PHPExcel_export.php');


	$sex = array(1=>"男", 2=>"女");
	$db = new Mysql($dbConfig);

	if (isset($_GET['type']) && trim($_GET['type'])==1){
		$sheetTitle = "members";
		$table = "users";
		$field = array('A'=>'账号', 
					   'B'=>'姓名', 
					   'C'=>'性别', 
					   'D'=>'电话', 
					   'E'=>'电子邮箱');

		$dataList = getList(array('table'=>$table,'where'=>" `mid`=2 "), $db);
		foreach ($dataList as &$data) {
			$temp = array();
			$temp['A'] = $data['username'];
			$temp['B'] = $data['realname'];
			$temp['C'] = $data['sex'];
			$temp['D'] = $data['phone'];
			$temp['E'] = $data['email'];
			$data = $temp;
		}
	}

	PHPExcelExport::getExportPage(0,$sheetTitle,1,$field,2,$dataList);

?>