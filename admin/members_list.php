<?php 
	/**
	 * @desc 所有用户列表页
	 */

	//公共部分
	include_once('./admin_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'member_list.css');
	$jsArr = array('member_list.js');
	$show_pageBox = true;

	$memberNum = $db->getNums(array('table'=>'users', 'where'=>" `mid`=2 ",'key'=>'uid'));
	$memberList = getList(array('table'=>'users','where'=>" `mid`=2 ", 'limit'=>'0,'.EACHPAGE), $db);

	//导出数据
	if (isset($_GET['download']) && trim($_GET['download'])=="true") {
		$memberList = getList(array('table'=>$usersTable,'where'=>" `mid`=2 "), $db);
		$field = array('username'=>'账号', 'realname'=>'姓名', 'sex'=>'性别', 'phone'=>'电话', 'email'=>'电子邮箱');
		$option = array('title'=>"information", 'field'=>$field, 'sex'=>$sex);
		downloadData($option, $memberList);
	}

	//获取搜索结果
	if (isset($_GET['key']) && $_GET['key']) {
		$option = array("table"=>"users", "key"=>$_GET['key']);
		$memberList = searchMember($option, $db);
		$show_pageBox = false;
	}

	//翻页
	$totalPage = ceil($memberNum/EACHPAGE);
	$currentPage = 1;
	if (isset($_GET['page']) && intval($_GET['page'])>=1) {
		$currentPage = $_GET['page'];
		$option = array("table"=>"users",'where'=>" `mid`=2 ","limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE);
		$memberList = getList($option, $db);
	}

	$url = SITE_URL."admin/members_list.php?page=";
	$pageBox = pageBox($totalPage,$currentPage, $url);
	$assignVar = array( "show_pageBox"=>$show_pageBox,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"sex"=>$sex,
						"memberList"=>$memberList,
						"pageBox"=>$pageBox,
						);
	$smarty->assign($assignVar);

	$smarty->display(ADMIN_TEM_DIR."members_list.html");
?>
