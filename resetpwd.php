<?php
	/**
	 * @desc 修改密码操作
	 */

	//公共部分
	include_once('./global.php');
	
	$userInfo = $base->auth();

	if ($_POST) {

		//获取用户详细信息
		$table = 'users';
		$userName  = $userInfo['userName'];
		$where = " `username`='$userName' ";
		$filter = array('table'=>$table, 'where'=>$where);
		$memberDetail = $db->getOne($filter);

		$oldPwd = trim($_POST['oldPwd']);
		$newPwd = trim($_POST['newPwd']);
		$confirmPwd = trim($_POST['confirmPwd']);

		if($newPwd != $confirmPwd)
			$base->ajax(false, "两次输入的密码不一致!");
		$salt = intval($memberDetail['salt']);
		$pwd = $db->password($salt, $oldPwd);
		if($pwd != $memberDetail['password'])
			$base->ajax(false, "原始密码输入错误!");
		
		//插入新密码
		$salt   = $db->salt();
		$newPwd = $db->password($salt, $newPwd);
		$where = " where `uid`=".$memberDetail['uid'];
		$filter = array('table'=>$table, 'where'=>$where);
		$data = array('salt'=>$salt, 'password'=>$newPwd);
		if(!$db->update($filter, $data))
			$base->ajax(false, "密码修改失败，请重新操作");
		$base->ajax(true, "密码修改成功!");
	}


	$cssArr = array('common.css', 'resetpwd.css');
	$jsArr = array('reset.js');

	$assignVar = array(
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						);
	$smarty->assign($assignVar);

	$smarty->display("resetpwd.html");
?>

