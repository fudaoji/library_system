<?php

//$flag = 0;
if (isset($flag) && $flag) {
	include_once ('../configs/config.php');
	include_once ('../library/smarty/libs/Smarty.class.php');
	include_once ('../library/mysql.php');
	include_once ('../library/base.php');
	include_once ('./common.php');
	include_once ('../library/page.php');
	include_once ('../library/PHPExcel.php');
}else{
include_once ('./configs/config.php');
include_once ('./library/smarty/libs/Smarty.class.php');
include_once ('./library/mysql.php');
include_once ('./library/base.php');
}

$show_pageBox = false;
$sex = array(1=>"男", 2=>"女");
$yesorno = array(1=>"是", 0=>"否");
$isLend = array(1=>"借出", 0=>"未借");
$isShow = array(1=>"上架", 0=>"下架");

//数据表变量定义
$usersTable      = "users";
$bookTypeTable   = "book_type";
$bookDetailTable = "book_detail";
$memberBookTable = "member_book";
$systemMsgTable  = "system_msg";
$sixinTable      = "sixin";



$base = new Base;
$db = new Mysql($dbConfig);

//********启动smarty**********
$smarty = new smarty();

$smarty->template_dir	= $smarty_template_dir;
$smarty->compile_dir	= $smarty_compile_dir;
$smarty->config_dir		= $smarty_config_dir;
$smarty->cache_dir		= $smarty_cache_dir;
$smarty->caching		= $smarty_caching;
$smarty->left_delimiter = $smarty_delimiter[0];
$smarty->right_delimiter= $smarty_delimiter[1];
//$smarty->assign("t_dir",$smarty_template_dir);
$smarty->assign("app_path",APP_PATH);
$smarty->assign("site_url",SITE_URL);
$smarty->assign("css_path",CSS_PATH);
$smarty->assign("js_path",JS_PATH);
$smarty->assign("img_path",IMG_PATH);
$smarty->assign("site_url",SITE_URL);

?>