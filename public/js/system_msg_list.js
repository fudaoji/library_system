$(function(){
	var popBox = document.getElementsByClassName('popBox');
	var canva  = document.getElementsByClassName('canva');
	$('.addItemBtn').click(function(){
		popBox[0].style.display = "block";
		canva[0].style.display = "block";
		$('#title').focus();

		$(".submitBtn").click(function(){
			var title = $('#title').val();
			var content = $('#content').val();
			var isShow = $('#isShow').val();

			if (title=="") {
				alert("请输入消息标题!");
				$('#title').focus();
				return false;
			};

			if (content=="") {
				alert("请输入消息内容!");
				$('#content').focus();
				return false;
			};

			$.post("/admin/system_msg_add.php",{title:title,content:content,isShow:isShow},function(data){
				if (data.status==true) {
					alert(data.info);
					location.href = '/admin/system_msg_list.php';
				}else{
					alert(data.info);
				}
			}, "json");		
		})
	});

});