$(function(){

	$('.searchBtn').click(function(){
		var tid = $('#tid').val();
		tid = (tid!=0) ? tid : null;
		var title = $('#title').val() ? $('#title').val() : null;

		if (!tid && !title) {
			alert("查询条件不能为空");
			return false;
		};

		var key = "";
		if(tid && !title) key = key+"tid="+tid;
		if(title && !tid) key = key+"title="+title;
		if(tid && title) key = key+"tid="+tid+"&title="+title;

		window.location.href = "/member/reserve_book.php?"+key;
	});

	$('.yudingBtn').click(function(){
		var bid = $(this).attr('name');
		var userName = $("#userName").val();

		//alert(userName);return false;
		$.post('/member/reserve_save.php',{bid:bid,userName:userName},function(data){
			if (data.status==true) {
				alert(data.info);
				location.href = '/member/reserved_books_list.php';
			}else{
				alert(data.info);
			}
		}, "json");	
	});
});