$(function() {
	$('#oldPwd').focus();
	$('#resetBtn').click(function() {
		var oldPwd      = $('#oldPwd').val();
		var newPwd      = $('#newPwd').val();
		var confirmPwd  = $('#confirmPwd').val();

		if (oldPwd == '') {
			alert('请输入原始密码');
			$('#oldPwd').focus();
			return false;
		}

		if (newPwd == '') {
			alert('请输入新密码');
			$('#newPwd').focus();
			return false;
		}

		if (confirmPwd != newPwd) {
			alert('两次输入的密码不一致');
			$('#confirmPwd').focus();
			return false;
		}

		$.post('http://library.com/resetpwd.php',{oldPwd:oldPwd,newPwd:newPwd,confirmPwd:confirmPwd}, function(data) {
			if (data.status == true) {
				alert(data.info);
				location.href = 'http://library.com';
			} else {
				alert(data.info);
			}
		}, 'json');
	});
});