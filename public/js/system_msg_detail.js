$(function() {
	$('#saveBtn').click(function() {
		var id       = $('#id').val();
		var title    = $('#title').val();
		var content  = $('#content').val();
		var isShow   = $('#isShow').val();

		if (title=="") {
			alert("请输入消息标题!");
			$('#title').focus();
			return false;
		};

		if (content=="") {
			alert("请输入消息内容!");
			$('#content').focus();
			return false;
		};

		$.post('/admin/system_msg_save.php',{id:id,title:title,content:content,isShow:isShow}, function(data) {
			if (data.status == true) {
				alert(data.info);
				history.go(0);
			} else {
				alert(data.info);
			}
		}, 'json');
	});
});