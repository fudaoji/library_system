$(function() {
	$('#saveBtn').click(function() {
		var id       = $('#id').val();
		var bid      = $('#bid').val();
		var title    = $('#title').val();
		var author   = $('#author').val();
		var description = $('#description').val();
		var tid      = $('#tid').val();
		var pubtime  = $('#pubtime').val();
		var isShow   = $('#isShow').val();
		var isLend   = $('#isLend').val();

		if (bid == '') {
			alert('请输入图书号');
			$('#bid').focus();
			return false;
		}

		if (title == '') {
			alert('请输入书名名');
			$('#title').focus();
			return false;
		}

		$.post('/admin/book_save.php',{id:id,bid:bid,title:title,author:author,description:description,tid:tid,pubtime:pubtime,isShow:isShow,isLend:isLend}, function(data) {
			if (data.status == true) {
				alert(data.info);
				history.go(0);
			} else {
				alert(data.info);
			}
		}, 'json');

	});
});