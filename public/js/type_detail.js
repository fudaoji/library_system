$(function() {
	$('#saveBtn').click(function() {
		var id       = $('#id').val();
		var typeName = $('#typeName').val();
		var typeID   = $('#typeID').val();

		if (typeID == '') {
			alert('请输入类别号');
			$('#typeID').focus();
			return false;
		}

		if (typeID == '') {
			alert('请输入类型名');
			$('#typeName').focus();
			return false;
		}

		$.post('/admin/type_save.php',{id:id,typeName:typeName,typeID:typeID}, function(data) {
			if (data.status == true) {
				alert(data.info);
				history.go(0);
			} else {
				alert(data.info);
			}
		}, 'json');
	});
});