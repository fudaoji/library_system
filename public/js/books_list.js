$(function(){
	var popBox = document.getElementsByClassName('popBox');
	var canva  = document.getElementsByClassName('canva');
	$('.addItemBtn').click(function(){
		popBox[0].style.display = "block";
		canva[0].style.display = "block";
		$('#bid').focus();

		$(".submitBtn").click(function(){
			var title = $('#title').val();
			var bid   = $('#bid').val();
			var tid   = $('#typeID').val();
			var author = $('#author').val();
			var description = $('#description').val();
			var pubtime = $('#pubtime').val();
			var isShow = $('#isShow').val();

			if (bid=="") {
				alert("请输入图书号!");
				$('#bid').focus();
				return false;
			};

			if (title=="") {
				alert("请输入图书名!");
				$('#title').focus();
				return false;
			};
			//alert(pubtime);return false;

			$.post("/admin/book_add.php",{bid:bid,tid:tid,title:title,author:author,description:description,pubtime:pubtime,isShow:isShow},function(data){
				if (data.status==true) {
					alert(data.info);
					location.href = '/admin/books_list.php?tid='+tid;
				}else{
					alert(data.info);
					$('#bid').val('');
					$('#bid').focus();
				}
			}, "json");		
		})
	});

	$('.delBtn').click(function() {
		var id = $(this).attr('name');
		var tid   = $('#typeID').val();

		if(confirm("真的要删除本书的数据库资料么？")){
			
			$.post('/admin/book_delete.php',{id:id,tid:tid}, function(data) {
				if (data.status == true) {
					alert(data.info);
					location.href = '/admin/books_list.php?tid='+tid;
				} else {
					alert(data.info);
				}
			}, 'json');
		}
		else{};
	});

	$('.searchBtn').click(function(){
		var searchKey = $('#searchKey').val();
		var tid   = $('#typeID').val();

		if (searchKey == '') {
			alert("请输入查询关键字");
			$('#searchKey').focus();
			return false;
		};

		window.location.href = "/admin/books_list.php?tid="+tid+"&key="+searchKey;
	})
});