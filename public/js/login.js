$(function() {
	$('#userName').focus();
	$('#loginBtn').click(function() {
		var userName = $('#userName').val();
		var pwd      = $('#passWord').val();
		var code     = $('#code').val();

		if (userName == '') {
			alert('请输入用户名');
			$('#userName').focus();
			return false;
		}
				
		if (pwd == '') {
			alert('请输入密码');
			$('#passWord').focus();
			return false;
		}

		if (code == '') {
			alert('请输入验证码');
			$('#code').focus();
			return false;
		}

		if (code.length != 4) {
			alert('请输入正确的验证码');
			$('#code').focus();
			return false;
		}

		$.post('http://library.com/login.php',{userName:userName,pwd:pwd,code:code}, function(data) {
			if (data.status == true) {
				location.href = '/index.php';
			} else {
				alert(data.info);
			}
		}, 'json');
	});
});