$(function() {
	$('#saveBtn').click(function() {
		var uid      = $('#uid').val();
		var userName = $('#userName').val();
		var realName = $('#realName').val();
		var sex      = $('#sex').val();

		if (userName == '') {
			alert('请输入账号');
			$('#userName').focus();
			return false;
		}


		$.post('/admin/member_save.php',{uid:uid,userName:userName,realName:realName,sex:sex}, function(data) {
			if (data.status == true) {
				alert(data.info);
				history.go(0);
			} else {
				alert(data.info);
			}
		}, 'json');
	});
});