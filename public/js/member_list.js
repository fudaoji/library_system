$(function() {
	$('.searchBtn').click(function(){
		var searchKey = $('#searchKey').val();

		if (searchKey == '') {
			alert("请输入查询关键字");
			$('#searchKey').focus();
			return false;
		};

		window.location.href = "/admin/members_list.php?key="+searchKey;
	})

	$('.delBtn').click(function() {
		var uid = $(this).attr('name');

		if(confirm("真的要删除此用户么？")){
			$.post('/admin/member_delete.php',{uid:uid}, function(data) {
				if (data.status == true) {
					alert(data.info);
					location.href = '/admin/members_list.php';
				} else {
					alert(data.info);
				}
			}, 'json');
		}
		else{};
	});

	$('.outputBtn').click(function(){
		window.location.href = "/admin/download_data.php?type=1";
	})

	$('.uploadBtn').click(function(){
		window.location.href = "/admin/upload_data.php";
	})
});