$(function(){
	var popBox = document.getElementsByClassName('popBox');
	var canva  = document.getElementsByClassName('canva');
	$('.addItemBtn').click(function(){
		popBox[0].style.display = "block";
		canva[0].style.display = "block";
		$('#typeID').focus();

		$(".submitBtn").click(function(){
			var typeName = $('#typeName').val();
			var typeID   = $('#typeID').val();

			if (typeID=="") {
				alert("请输入类型号!");
				$('#typeID').focus();
				return false;
			};

			if (typeName=="") {
				alert("请输入类型名!");
				$('#typeName').focus();
				return false;
			};

			$.post("/admin/type_add.php",{typeID:typeID,typeName:typeName},function(data){
				if (data.status==true) {
					alert(data.info);
					location.href = '/admin/book_types_list.php';
				}else{
					alert(data.info);
					$('#typeID').val('');
					$('#typeID').focus();
				}
			}, "json");		
		})
	});

	$('.delBtn').click(function() {
		var id = $(this).attr('name');

		if(confirm("真的要删除此分类么？")){
			
			$.post('/admin/type_delete.php',{id:id}, function(data) {
				if (data.status == true) {
					alert(data.info);
					location.href = '/admin/book_types_list.php';
				} else {
					alert(data.info);
				}
			}, 'json');
		}
		else{};
	});

	$('.searchBtn').click(function(){
		var searchKey = $('#searchKey').val();

		if (searchKey == '') {
			alert("请输入查询关键字");
			$('#searchKey').focus();
			return false;
		};

		window.location.href = "/admin/book_types_list.php?key="+searchKey;
	})
});