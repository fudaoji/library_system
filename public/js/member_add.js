$(function() {
	$('#userName').focus();
	$('#addBtn').click(function() {
	var userName = $('#userName').val();
	var mid      = $('#mid').val();

	if (userName == '') {
		alert('请输入用户名');
		$('#userName').focus();
		return false;
	}
				
	if (mid == '0') {
		alert('请选择用户类型');
		$('#mid').focus();
		return false;
	}

	$.post('/admin/member_add.php',{userName:userName,mid:mid}, function(data) {
		if (data.status == true) {
			alert(data.info);
			history.go(0);
		} else {
			alert(data.info);
		}
	}, 'json');
});
});