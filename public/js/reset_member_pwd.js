$(function() {
	$('#resetBtn').click(function() {
		var userName    = $('#userName').val();
		var newPwd      = $('#newPwd').val();
		var confirmPwd  = $('#confirmPwd').val();

		if (userName == '') {
			alert('请输入用户账号');
			$('#userName').focus();
			return false;
		}

		if (newPwd == '') {
			alert('请输入新密码');
			$('#newPwd').focus();
			return false;
		}

		if (confirmPwd != newPwd) {
			alert('两次输入的密码不一致');
			$('#confirmPwd').focus();
			return false;
		}

		$.post('/admin/reset_member_pwd.php',{userName:userName,newPwd:newPwd,confirmPwd:confirmPwd}, function(data) {
			if (data.status == true) {
				alert(data.info);
				location.href = '/admin/reset_member_pwd.php';
			} else {
				alert(data.info);
			}
		}, 'json');
	});
});