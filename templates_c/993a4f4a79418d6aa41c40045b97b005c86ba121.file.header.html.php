<?php /* Smarty version Smarty-3.1.15, created on 2013-10-16 09:03:28
         compiled from ".\templates\header.html" */ ?>
<?php /*%%SmartyHeaderCode:157905258df4af396d5-59592495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '993a4f4a79418d6aa41c40045b97b005c86ba121' => 
    array (
      0 => '.\\templates\\header.html',
      1 => 1381829413,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '157905258df4af396d5-59592495',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_5258df4b034063_12201694',
  'variables' => 
  array (
    'title' => 0,
    'cssArr' => 0,
    'css_path' => 0,
    'css' => 0,
    'js_path' => 0,
    'userInfo' => 0,
    'site_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5258df4b034063_12201694')) {function content_5258df4b034063_12201694($_smarty_tpl) {?><html>
<head>
	<meta charset="utf-8">
	<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
	<?php  $_smarty_tpl->tpl_vars['css'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['css']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cssArr']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['css']->key => $_smarty_tpl->tpl_vars['css']->value) {
$_smarty_tpl->tpl_vars['css']->_loop = true;
?>
	<link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
<?php echo $_smarty_tpl->tpl_vars['css']->value;?>
">
	<?php } ?>

	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
jquery-1.9.1.min.js">
	</script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
common.js">
	</script>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
calendar/WdatePicker.js">
	</script>
</head>
<body>
	<!--遮罩层-->
	<div class="canva"></div>
	
	<div id="header">
		<div id="date">今天是&nbsp;<?php echo date("Y年m月d日",time());?>
</div>
		<center><div id="siteName">图书借阅系统</div></center>
		<div id="welcome">
			欢迎<?php if ($_smarty_tpl->tpl_vars['userInfo']->value['mid']==1) {?>管理员<?php }?> &nbsp;
			<em><?php echo $_smarty_tpl->tpl_vars['userInfo']->value['userName'];?>
</em>
		</div>
	</div>
	<div id="left_bar">
		<ul id="left_bar_nav">
			<li>
				<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
">
					&nbsp;&nbsp;&nbsp;&nbsp;==首页==
				</a>
			</li>
			
			<?php if ($_smarty_tpl->tpl_vars['userInfo']->value['mid']==1) {?>
			<li><<&nbsp;&nbsp;用户管理
				<ul class="nav_child">
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/members_list.php">
							*用户列表
						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/member_add.php">
							*添加用户
						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/reset_member_pwd.php">*修改用户密码
						</a>
					</li>
				</ul>
			</li>
			<li><<&nbsp;&nbsp;书籍管理
				<ul class="nav_child">
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/book_types_list.php">
							*所有分类
						</a>
					</li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/overtime_books_list.php">
							*超期书籍
						</a>
					</li>
				</ul>
			</li>
			<?php }?>
			
			<?php if ($_smarty_tpl->tpl_vars['userInfo']->value['mid']==2) {?>
			<li><<&nbsp;&nbsp;个人信息管理
				<ul class="nav_child">
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
member/info.php">*我的个人信息</a>
					</li>
				</ul>
			</li>
			<li><<&nbsp;&nbsp;管理我的图书
				<ul class="nav_child">
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
member/reserved_books_list.php">*我的书籍订单</a>
					</li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
member/reserve_book.php">*预定图书</a></li>
					<li>
						<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
member/overtime_books_list.php">*图书催还</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
member/sixin_list.php">
					<<&nbsp;&nbsp;我的私信
				</a>
			</li>
			<?php }?>
			<li><<&nbsp;&nbsp;系统操作
				<ul class="nav_child">
					<?php if ($_smarty_tpl->tpl_vars['userInfo']->value['mid']==1) {?>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
admin/system_msg_list.php">*系统消息</a></li>
					<?php } else { ?>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
member/system_msg.php">*系统消息</a></li>
					<?php }?>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
resetpwd.php">*修改密码</a></li>
					<li><a href="<?php echo $_smarty_tpl->tpl_vars['site_url']->value;?>
logout.php">*退出</a></li>
				</ul>
			</li>
		</ul>
	</div>
<?php }} ?>
