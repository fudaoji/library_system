<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 09:21:38
         compiled from ".\templates\resetpwd.html" */ ?>
<?php /*%%SmartyHeaderCode:13424525c98a28edb56-73779830%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2eb1ac391cf1f570c0d4fb1c34cb9e1e282fda0a' => 
    array (
      0 => '.\\templates\\resetpwd.html',
      1 => 1381480076,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13424525c98a28edb56-73779830',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525c98a2a58689_97353540',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525c98a2a58689_97353540')) {function content_525c98a2a58689_97353540($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ("header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"修改密码"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="softFont">修改密码</span>
		</div>
		<div id="main_body">
			<center>
				<form id="resetForm">
					<div id="formTitle">修改我的密码</div>
					<table cellpadding="5" cellspacing="5">
						<tr>
							<th><label for="oldPwd">旧&nbsp;密&nbsp;码:</label></th>
							<td><input type="password" id="oldPwd" name="oldPwd" /></td>
						</tr>
						<tr>
							<th><label for="newPwd">新&nbsp;密&nbsp;码:</label></th>
							<td><input type="password" id="newPwd" name="newPwd" /></td>
						</tr>
						<tr>
							<th><label for="confiemPwd">确认密码:</label></th>
							<td>
								<input type="password" id="confirmPwd" name="confirmPwd" />
							</td>
						</tr>
					</table>
					<div id="resetBtn">提交</div>
				</form>
			</center>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }} ?>
