<?php 
	include_once('./global.php');

	$cssArr = array('login.css', 'common.css');
	$jsArr  = array('login.js');

	if (isset($_SESSION['userName'])) {
		$_SESSION['userName'] && header("Location: ".SITE_URL);
	}
	 
	if ($_POST) {
	
		$userName = trim($_POST['userName']);
		$password = trim($_POST['pwd']);
		$code     = trim($_POST['code']);

		($_SESSION['valicode'] != $code) && $base->ajax(false, "验证码输入错误");
		
		$table = 'users';
		$where = "`username`='{$userName}' ";
		$filter = array('table'=>$table, 'where'=>$where);
		if (!$user=$db->getOne($filter)) 
			$base->ajax(false, '用户不存在');

		if($user['password']!=$db->password($user['salt'],$password)) 
			$base->ajax(false, "密码错误");

		$_SESSION['userName']  = $user['username'];
		$_SESSION['mid']   = $user['mid'];

		$base->ajax(true);
	}

	$smarty->assign("cssArr", $cssArr);
	$smarty->assign("jsArr", $jsArr);

	$smarty->display("login.html");

?>