<?php
	/**
     * @获取验证码
     * @return img
     */

		session_start();  //将验证码上的内容保存在session上

		$text = $rand = '';

	 	for ($i=0; $i < 4; $i++) {
	 		/*生成随机数*/
	 		$char  = dechex(rand(1, 15));
			$rand .= $char;
			$text .= ' '.$char;
		}

		$_SESSION['valicode'] = $rand;

		//创建图片
		$width = 80; $height = 30;
		$img = imagecreatetruecolor($width, $height);

		//设置颜色
		$bgColor = imagecolorallocate($img, 0, 0, 0);
		$fontColor = imagecolorallocate($img, 255, 255, 255);

		//将随机数写入到图片中
		imagestring($img, 6, rand(2, 5), rand(2, 8), $text, $fontColor);

		//画干扰线
		/*for ($i = 0; $i < 2 ; $i++) {
			$randColor = imagecolorallocate($img, rand(10,255), rand(10,255), rand(10,255));
			imageline($img, 0,rand(0,30), 100, rand(0,30), $randColor);
		}*/

		//画噪点
		//imagesetpixel(图像句柄,x, y, color)
		for ($i = 0; $i < 100; $i++) {
			$randColor = imagecolorallocate($img, rand(10,255), rand(10,255), rand(10,255));
			imagesetpixel($img, rand()%100, rand()%30, $randColor);
		}

		//输出图像
		header("Content-Type: image/jpeg");
		imagejpeg($img);
	
