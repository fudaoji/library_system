<?php

//require_once('E:/www/library_system/config/config.php');

class Base{

	public $userInfo=array();

	/**
	 * @desc 初始化操作
	 */
	public function __construct(){
		session_start();
	}

	/**
	 * @desc 验证用户登录状态
	 */
	public function auth(){

		if (!isset($_SESSION['userName']) && empty($_SESSION['userName'])) {
			header("Location: ".SITE_URL."login.php");
			exit();
		}
		$this->userInfo = array('userName' => $_SESSION['userName'],
					  			'mid' => $_SESSION['mid'] );
		return $this->userInfo;
	}

	/**
     * AJAX返回
     * @param string $msg 提示信息
     * @param int $status 返回状态
     * @param mixed $data 要返回的数据
     * @param string $type ajax返回类型
     */
	public function ajax($status=1, $msg=null, $data=array(), $type='json'){
		# 编码
		header("Content-Type:text/html; charset=utf-8");
		# 信息
		$result = array('status'=>$status, 'info'=>$msg, 'data'=>$data);
		# 格式
		'json' == $type && exit(json_encode($result));
		'xml' == $type && exit(xml_encode($result));
		'eval' == $type && exit($data);
	}
}