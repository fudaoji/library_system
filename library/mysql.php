<?php
/**
 * @desc 数据库类
 */

include_once(dirname(__FILE__).'/../configs/config.php');

class Mysql{

	protected $link;
	protected $_config = array();
	public $error;
	public $table='';

	public function __construct($dbConfig){
		$this->_config = $dbConfig;
		$this->connect();
	}

	/**
	 * @desc数据库连接
	 */
	public function connect(){
		if (!$this->link) {
			try{
				$this->link=new PDO($this->_config['dsn'],$this->_config['user'],$this->_config['password']);
			}catch(PDOException $e){
			   die("Error!: ".$e->getMessage()."<br/>");
			}
		}
	}

	public function exec($sql){
		$result = $this->link->exec($sql);
		return $result;
	}

	public function query($sql){
		$query = $this->link->query($sql);
		if (!$query) {
			return array();
		}
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	/**
	 *@desc 获取一条记录
	 *@param array $filter 过滤条件
	 *@return array 结果数组
	 */
	public function getOne($filter){
		$sql = "select * from `".$filter['table']."` where ".$filter['where']." limit 1";
		if (!$result = $this->query($sql)) {
			return array();
		}
		
		return $result[0];
	}

	/**
	 *@desc 获取所有记录
	 *@param array $filter 过滤条件
	 *@return array 结果数组
	 */
	public function getAll($filter){
		$where = " where ";
		if(isset($filter['where']) && $filter['where'])
			$where .= $filter['where'];
		$sql = "select * from `".$filter['table']."`".$where;
		if (!$result = $this->query($sql)) {
			return array();
		}
		
		return $result;
	}

	/**
	 *@desc 获取多条记录
	 *@param array $filter 过滤条件
	 *@return array 结果数组
	 */
	public function getSome($filter, $limit=false){
		$where = "";
		$orderBy = "";
		$limit = $limit ? " limit ".$limit : '';
		if(isset($filter['where']) && $filter['where'])
			$where .= " where ".$filter['where'];
		if(isset($filter['orderby']) && $filter['orderby'])
			$orderBy .= " order by ".$filter['orderby'];
		$sql = "select * from `".$filter['table']."`".$where." ".$orderBy.$limit;
		if (!$result = $this->query($sql)) {
			return array();
		}
		return $result;
	}

	/**
	 *@desc 添加记录
	 *@param array $data 要插入的内容
	 *@return int 影响行数
	 */
	function insert($table, $data){
		if($this->saveSql($data)){
			$fields = join(',', array_keys($data));  //字段数组
			$vals = join("','", $data);
			$sql  = "insert into " .$table." ($fields) VALUES ('$vals')";
			if($result = $this->exec($sql)){
				return $result;
			}
		}
		return false;
	}

	/**
	 *@desc 删除记录
	 *@param array $filter 过滤条件
	 *@return int 影响行数
	 */
	function delete($filter){
		$where = " where ";
		if(isset($filter['where']) && $filter['where'])
			$where .= $filter['where'];
		$sql = "delete from `".$filter['table']."`".$where;
	
		return $this->exec($sql);
	}

	/**
	 *@desc 更新记录
	 *@param $filter 过滤条件
	 *@param array $data 要插入的内容
	 *@return int 影响行数
	 */
	function update($filter, $data){
		if($this->saveSql($data)){
			$fields = array();
			foreach ($data as $key => $value) {
				if(is_string($value))
					$fields[] = "`{$key}`='{$value}'";
				else
					$fields[] = "`{$key}`={$value}";
			}
			
			$fields = join(',', $fields);
			$sql  = "update `" .$filter['table']."` set ".$fields.$filter['where'];
			if($result = $this->exec($sql)){
				return $result;
			}
		}
		return false;
	}

	/**
	 * @desc获取记录条数
	 */
	public function getNums($filter){
		$where="";
		$key = $filter['key'];
		$table = $filter['table'];
		if (isset($filter['where'])) {
			$where .= " where ".$filter['where'];
		}
		$sql = "select count(`".$key."`) as nums from `".$table."`".$where;
		$result = $this->query($sql);
		$num = $result ? $result[0]['nums'] : 0;
		return $num;
	}

	/**
	 *@desc 过滤危险数据
	 * @param array $data 要过滤的键值对
	 */
	private function saveSql(&$data){
		foreach($data as $k => &$v){
			$v = strtr($v, array('\\' => '', "'" => "\'"));
		}
		return $data? true: false;
	}

	/**
	 * @desc 密码加密
	 *@param int $salt 加密参数
	 *@param string $pwd 原始密码
	 *@return 返回加密后密码
	 */
	public function password($salt=null, $pwd){
		return md5($pwd.$salt);
	}

	/**
	 *@desc利用随机数生成加密参数
	 *@return int 加密参数
	 */
	public function salt(){
		return rand(1000,9999);
	}
}
//print_r($dbConfig);exit;
/*$sql = "select * from `users`";*/
//$data = array('username'=>'fdj','realname'=>'jj', 'mid'=>'1', 'salt'=>'2345', 'password'=>'123456');
//$mysql = new Mysql($dbConfig);
//$filter = array('table'=>'users', 'key'=>"uid");
//$data = array('sender'=>'admin', 'receiver'=>'102', 'sixin'=>"ads", 'sendtime'=>12123123);
/*$result = $mysql->insert('sixin', $data);
if ($result) {
	print_r($result);
}else{
	echo $result;
}*/

?>