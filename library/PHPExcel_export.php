<?php

	class PHPExcelExport{

		private static $objPHPExcel = null;
		private static $objPhpSheet = null;

		function __construct(){
			date_default_timezone_set('Asia/Shanghai');
			if (PHP_SAPI == 'cli') die('请使用浏览器进行操作！');
		}

		public  static function getExportPage($sheetIndex,$sheetTitle,$index,$field,$dataIndex,$dataList){
			PHPExcelExport::createSheet($sheetIndex,$sheetTitle);
			PHPExcelExport::setFirstRow($index,$field);
			PHPExcelExport::setDataRow($dataIndex,$dataList);
			PHPExcelExport::saveExport($sheetTitle);
		}

		public static function getInstance(){
			include_once ('../library/PHPExcel.php');
			if (self::$objPHPExcel==null) {
				self::$objPHPExcel = new PHPExcel();
			}
			return self::$objPHPExcel;
		}

		public static function createSheet($sheetindex=0,$sheetTitle){
	        self::getInstance();
	        self::$objPHPExcel->createSheet();
			self::$objPHPExcel->setActiveSheetIndex($sheetindex);
			self::$objPhpSheet = self::$objPHPExcel->getActiveSheet();
			self::$objPhpSheet->setTitle($sheetTitle);
	    }

		public static function setFirstRow($k,$field){
			self::getInstance();
	        foreach($field as $key=> $val){
	            self::$objPhpSheet->setCellValue($key.$k, $val);
	        }
	    }

	    public static function setDataRow($index,$dataList){
	    	self::getInstance();
	    	foreach ($dataList as $data) {
	    		foreach ($data as $k => $v) {
	    			self::$objPhpSheet->setCellValue($k . $index, $v);
	    		}
	    		$index++;
	    	}
	    }

	    public static function saveExport($sheetTitle){

			//设置文件类型
			header('Content-Type: application/vnd.ms-excel;');
			//设置默认的文件名 
			header('Content-Disposition: attachment;filename="'.$sheetTitle.'.xls"'); 
			header('Cache-Control: max-age=0');
			// 针对 IE 9
			header('Cache-Control: max-age=1');
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter(self::$objPHPExcel, 'Excel5');
			$objWriter->save("php://output");
			exit();
	    }
	}

?>