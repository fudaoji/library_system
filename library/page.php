<?php
/**
 * @desc 分页操作
 */

function getList($option, $db){
	$filter = array('table'=>$option['table']);
	if(isset($option['where']))
		$filter['where'] = $option['where'];
	if(isset($option['orderby']))
		$filter['orderby'] = $option['orderby'];
	$limit = isset($option['limit']) ? $option['limit'] : null;
	return $db->getSome($filter, $limit);
}

function pageBox($totalPage=1, $currentPage=1, $url){

	$html = "<ul><a href='".$url."1'><li>首页</li></a>";
	if ($currentPage>1) {
		$html .= "<a href='".$url.($currentPage-1)."'><li>上一页</li></a>";
	}

	$num = ($totalPage>5) ? 5 : $totalPage;
	for ($i=1; $i <= $num; $i++) { 
		$html .= "<a href='".$url.$i."'><li>{$i}</li></a>";
	}

	($totalPage>5) && $html .= "..."; 

	if ($currentPage<$totalPage) {
		$html .= "<a href='".$url.($currentPage+1)."'><li>下一页</li></a>";
	}

	$html .= "<a href='".$url.$totalPage."'><li>尾页</li></a></ul>";
	return $html;
}

?>
