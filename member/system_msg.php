<?php 
	/**
	 * @desc 查看系统消息页
	 */

	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'system_msg.css');
	$jsArr = array('system_msg.js');

	$filter = array('table'=>$systemMsgTable, "where"=>" is_show=1 ");
	$systemMsg = $db->getOne($filter);
	
	$assignVar = array(
						"systemMsg"=>$systemMsg,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						);

	$smarty->assign($assignVar);

	$smarty->display(MEMBER_TEM_DIR."system_msg.html");
?>
