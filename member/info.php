<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'info.css');
	$jsArr = array('info.js');

	$option = array('table'=>$usersTable, 'where'=>array('username'=>$userInfo['userName']));
	$info = isExist($option, $db);

	if ($_POST) {
		$uid      = intval($_POST['uid']);
		$realName = trim($_POST['realName']);
		$sex      = intval($_POST['sex']);
		$phone    = trim($_POST['phone']);
		$email    = trim($_POST['email']);

		$option = array('table'=>$usersTable,'where'=>array('uid'=>$uid));
		$data = array('realname'=>$realName, 
					  'sex'=>$sex,
					  'phone'=>$phone,
					  'email'=>$email,);

		if (saveInfo($option, $data, $db)) {
			$base->ajax(true, "更新信息成功");
		}
		$base->ajax(false, "更新信息失败，请重新操作");
	}

	//print_r($info);exit;
	
	$assignVar = array(	"info"=>$info,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"sex"=>$sex,
						);

	$smarty->assign($assignVar);

	$smarty->display(MEMBER_TEM_DIR."info.html");
?>
