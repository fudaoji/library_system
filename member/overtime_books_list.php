<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'overtime_books_list.css');
	$jsArr = array('overtime_books_list.js');
	
	$date = time();  //今日时间戳
	$dateline = $date + 432000;  //最后期限5天
	$reserves = getList(array('table'=>$memberBookTable,'where'=>"username='".$userInfo['userName']."' and end_time<=".$dateline), $db);

	//print_r($reserves);exit;
	$bookList = array();
	foreach ($reserves as $value) {
		$option = array('table'=>$bookDetailTable, 'where'=>array('bid'=>$value['bid']));
		$book = isExist($option, $db);
		if($value['end_time']>$date){
			$book['isOver']=0;
			$book['day'] = date("d", $value['end_time']-$date);
		}else {
			$book['isOver'] = 1;
			$book['day'] = date("d", $date-$value['end_time']);
		}
		$bookList[] = $book;
	}
	
	$assignVar = array(
						"bookList"=>$bookList,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						);

	$smarty->assign($assignVar);

	$smarty->display(MEMBER_TEM_DIR."overtime_books_list.html");
?>
