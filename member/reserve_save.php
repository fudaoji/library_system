<?php
/**
 * @desc 保存预定信息
 */
	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();
	
	if ($_POST) {
		
		$bid = trim($_POST['bid']);
		$userName = trim($_POST['userName']);

		$begin_time =  time();
		//$end_time = $begin_time + 518400;
		$end_time = $begin_time + 345600;

		$data = array("bid"=>$bid, 
					  "username"=>$userName,
					  "begin_time"=>$begin_time,
					  "end_time"=>$end_time,
					  );

		//插入数据
		if (!$result=$db->insert($memberBookTable, $data)) 
			$base->ajax(false, '预定失败,请重新操作');

		$data = array("bid"=>$bid, 
					  "is_lend"=>1,
					  );

		$option = array('table'=>$bookDetailTable,'where'=>array('bid'=>$bid));
		
		//更新书籍出借状态
		saveInfo($option, $data, $db);

		$base->ajax(true, "预定成功!");
	}

?>