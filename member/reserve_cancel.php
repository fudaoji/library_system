<?php
/**
 * @desc 保存预定信息
 */
	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();
	
	if ($_POST) {
		
		$bid = trim($_POST['bid']);
		//$userName = trim($_POST['userName']);

		$option = array('bid'=>$bid);
		if (delObj($memberBookTable, $option, $db)) {
			$data = array("is_lend"=>0);
			$option = array('table'=>$bookDetailTable,'where'=>array('bid'=>$bid));
			//更新书籍出借状态
			saveInfo($option, $data, $db);
			$base->ajax(true, "退订成功!");
		}
		$base->ajax(false, "退订失败，请重新操作!");
	}

?>