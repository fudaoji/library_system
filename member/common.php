<?php

/**
 * @desc 普通用户操作公共模块
 */

/**
 * @desc根据搜索条件获取书籍列表
 */
function searchBooks($option, $db){
	$where = array();
	foreach ($option['where'] as $key => $value) {
		if(is_string($value)) $where[] = "`$key` like '%".$value."%'";
		else $where[] = "`$key`=$value";
	}
	$where = join(' and ', $where);
	$filter = array('table'=>$option['table'], 'where'=>$where);

	$result=$db->getAll($filter);
	return array_values($result);
}

	/**
	 * @desc更改信息
	 */

	function saveInfo($option, $data, $db){
		$where = array();
		foreach ($option["where"] as $key => $value) {
			if(is_string($value))
				$where[] = "`$key`='$value'";
			else
				$where[] = "`$key`=$value";
		}
		$where = join(' and ', $where);
		$where = " where ".$where." ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		return $db->update($filter, $data);
	}

	/**
	 * @desc判断对象是否存在
	 */

	function isExist($option, $db){
		$where = array();
		foreach ($option["where"] as $key => $value) {
			if(is_string($value))
				$where[] = "`$key`='$value'";
			else
				$where[] = "`$key`=$value";
		}
		$where = join(' and ', $where);
		$where = " ".$where." ";
		$filter = array('table'=>$option['table'], 'where'=>$where);
		if ($obj=$db->getOne($filter)) 
			return $obj;
		return 0;
	}

	/**
	 * @desc删除指定对象
	 */
	function delObj($table, $option, $db){
		foreach ($option as $key => $value) {
			if(is_string($value)) $where = " `$key`='$value' ";
			else $where = " `$key`=$value ";
		}
		$filter = array('table'=>$table, 'where'=>$where);
		return $db->delete($filter);
	}

?>