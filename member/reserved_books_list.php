<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'reserved_books_list.css');
	$jsArr = array('reserved_books_list.js');
	

	$reserves = getList(array('table'=>$memberBookTable,'where'=>"username='".$userInfo['userName']."'"), $db);

	//print_r($reserves);exit;
	$bookList = array();
	foreach ($reserves as $value) {
		$option = array('table'=>$bookDetailTable, 'where'=>array('bid'=>$value['bid']));
		$book = isExist($option, $db);
		$bookList[] = $book;
	}
	
	$assignVar = array(
						"bookList"=>$bookList,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						);

	$smarty->assign($assignVar);

	$smarty->display(MEMBER_TEM_DIR."reserved_books_list.html");
?>
