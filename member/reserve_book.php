<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'reserve_book.css');
	$jsArr = array('reserve_book.js');
	
	$pageBox = '';
	$typeList = getList(array('table'=>$bookTypeTable), $db);
	$bookList = array();

	//获取搜索结果
	if ($_GET) {

		$show_pageBox = true;
		$keyArr = $_GET;
		$option = array("table"=>$bookDetailTable);
		$where = array();
		
		isset($keyArr['tid']) && $where['tid'] = $keyArr['tid'];
		isset($keyArr['title']) && $where['title'] = $keyArr['title'];
		$where['is_show'] = 1;
		$option['where'] = $where;
		//print_r($_SERVER);exit;
		$bookList = searchBooks($option, $db);
		$bookNum = count($bookList);
		$bookList = array_slice($bookList, 0, EACHPAGE);
		
		//翻页
		$totalPage = ceil($bookNum/EACHPAGE);
		$currentPage = 1;
		
		$request_url = 'member/reserve_book.php?';
		$where = $url_query = array();
		unset($keyArr['page']);
		foreach ($keyArr as $key => $value) {
			$url_query[] = "$key=$value";
			if(is_string($value)) $where[] = "`$key`='$value'";
			else $where[] = "`$key`=$value";
		}
		$where = join(' and ', $where).' and `is_show`=1';
		$url_query = join('&', $url_query);
		
		if (isset($_GET['page']) && intval($_GET['page'])>=1) {
			$currentPage = $_GET['page'];
			$option = array("table"=>$bookDetailTable,
							"where"=>$where,
							"limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE
							);
			$bookList = getList($option, $db);
		}
		
		$url = SITE_URL.$request_url.$url_query."&page=";
		$pageBox = pageBox($totalPage,$currentPage, $url);
	}

	$assignVar = array( "typeList"=>$typeList,
						"bookList"=>$bookList,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"show_pageBox"=>$show_pageBox,
						"pageBox"=>$pageBox,
						);

	$smarty->assign($assignVar);

	$smarty->display(MEMBER_TEM_DIR."reserve_book.html");
?>
