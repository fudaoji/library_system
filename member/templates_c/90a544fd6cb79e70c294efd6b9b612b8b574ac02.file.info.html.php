<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 18:13:52
         compiled from "E:\www\library_system\templates\member\info.html" */ ?>
<?php /*%%SmartyHeaderCode:30076525d10992f8473-02511648%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90a544fd6cb79e70c294efd6b9b612b8b574ac02' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\member\\info.html',
      1 => 1381832021,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '30076525d10992f8473-02511648',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525d1099372540_55879147',
  'variables' => 
  array (
    'info' => 0,
    'sex' => 0,
    'k' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525d1099372540_55879147')) {function content_525d1099372540_55879147($_smarty_tpl) {?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"我的个人信息"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">个人信息管理</span> >> 
			<span class="softFont">我的个人信息</span>
		</div>
		<div id="main_body">
			<center>
				<form id="editForm">
					<div id="formTitle">
						我的个人信息
					</div>
					<table cellpadding="5" cellspacing="5">
						<tr>
							<th>账号</th>
							<td><?php echo $_smarty_tpl->tpl_vars['info']->value['username'];?>
</td>
						</tr>
						<tr>
							<th>真实姓名</th>
							<td>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['realname'];?>
" id="realName" />
							</td>
						</tr>
						<tr>
							<th>性别</th>
							<td>
								<select id="sex">
									<?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sex']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['v']->key;
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['k']->value==$_smarty_tpl->tpl_vars['info']->value['sex']) {?> selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value;?>

									</option>
									<?php } ?>
								</select>
							</td>
						</tr>
						<tr>
							<th>电话</th>
							<td>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['phone'];?>
" id="phone" />
							</td>
						</tr>
						<tr>
							<th>电子邮箱</th>
							<td>
								<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['email'];?>
" id="email" />
							</td>
						</tr>
					</table>
					<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['info']->value['uid'];?>
" id="uid" />
					<div id="saveBtn">保存</div>
				</form>
			</center>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
