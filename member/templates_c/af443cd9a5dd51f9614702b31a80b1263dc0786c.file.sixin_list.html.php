<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 16:46:30
         compiled from "E:\www\library_system\templates\member\sixin_list.html" */ ?>
<?php /*%%SmartyHeaderCode:17993525d000e810410-26744052%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af443cd9a5dd51f9614702b31a80b1263dc0786c' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\member\\sixin_list.html',
      1 => 1381826773,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17993525d000e810410-26744052',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525d000e84d463_12446016',
  'variables' => 
  array (
    'sixinList' => 0,
    'sixin' => 0,
    'userInfo' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525d000e84d463_12446016')) {function content_525d000e84d463_12446016($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"私信列表"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="softFont">
				我的私信
			</span>
		</div>
		<div id="main_body">
			<?php if ($_smarty_tpl->tpl_vars['sixinList']->value) {?>
			<center>
				<form id="listForm">
					<table id="sixinList" cellpadding="5" cellspacing="0">
						<tr>
							<th>时间</th>
							<th class="wider">内容</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['sixin'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sixin']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sixinList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sixin']->key => $_smarty_tpl->tpl_vars['sixin']->value) {
$_smarty_tpl->tpl_vars['sixin']->_loop = true;
?>
						<tr>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['sixin']->value['sendtime'],"%Y-%m-%d %H:%M:%S");?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['sixin']->value['sixin'];?>
</td>
						</tr>
						<?php } ?>
					</table>
					<input type="hidden" id="from" value="<?php echo $_smarty_tpl->tpl_vars['userInfo']->value['userName'];?>
" />
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<div class="noContent">暂无私信</div>
			<?php }?>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
