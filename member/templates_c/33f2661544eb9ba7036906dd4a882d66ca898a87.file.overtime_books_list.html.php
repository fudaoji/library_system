<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 10:49:46
         compiled from "E:\www\library_system\templates\member\overtime_books_list.html" */ ?>
<?php /*%%SmartyHeaderCode:5541525ca8ab1bcac1-88322883%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '33f2661544eb9ba7036906dd4a882d66ca898a87' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\member\\overtime_books_list.html',
      1 => 1381805378,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5541525ca8ab1bcac1-88322883',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525ca8ab236a52_08896617',
  'variables' => 
  array (
    'bookList' => 0,
    'book' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525ca8ab236a52_08896617')) {function content_525ca8ab236a52_08896617($_smarty_tpl) {?>

<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"图书催还"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="softFont">图书催还</span>
		</div>
		<div id="main_body">
			<?php if ($_smarty_tpl->tpl_vars['bookList']->value) {?>
			<center>
				<form id="listForm">
					<table id="bookList" cellpadding="5" cellspacing="0">
						<tr>
							<th>图书号</th>
							<th>图书名</th>
							<th>状态</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['book'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['book']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bookList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['book']->key => $_smarty_tpl->tpl_vars['book']->value) {
$_smarty_tpl->tpl_vars['book']->_loop = true;
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>

							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['title'];?>

							</td>
							<td>
								<?php if ($_smarty_tpl->tpl_vars['book']->value['isOver']) {?>
								已超期<?php echo $_smarty_tpl->tpl_vars['book']->value['day'];?>
天
								<?php } else { ?>
								还有<?php echo $_smarty_tpl->tpl_vars['book']->value['day']-1;?>
天到期
								<?php }?>
							</td>
						</tr>
						<?php } ?>
					</table>
				</form>
			</center>
			<?php } else { ?>
			<div class="noContent">没有将要到期书籍</div>
			<?php }?>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
