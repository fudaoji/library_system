<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 17:36:41
         compiled from "E:\www\library_system\templates\member\reserve_book.html" */ ?>
<?php /*%%SmartyHeaderCode:501525bd028ad82b9-25686348%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fd5a26bdef59a0a886c825f22fbf4dbe2834c19f' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\member\\reserve_book.html',
      1 => 1381829788,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '501525bd028ad82b9-25686348',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525bd028b8f3a2_43954932',
  'variables' => 
  array (
    'typeList' => 0,
    'type' => 0,
    'bookList' => 0,
    'book' => 0,
    'userInfo' => 0,
    'show_pageBox' => 0,
    'pageBox' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525bd028b8f3a2_43954932')) {function content_525bd028b8f3a2_43954932($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"图书预定"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">管理我的图书</span> >> 
			<span class="softFont">图书预定</span>
		</div>
		<div id="main_body">
			<div class="searchBox">
				类别:
				<select id="tid">
					<option value="0">--请选择--</option>
					<?php if ($_smarty_tpl->tpl_vars['typeList']->value) {?>
					<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['typeList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
?>
					<option value="<?php echo $_smarty_tpl->tpl_vars['type']->value['typeid'];?>
">
						<?php echo $_smarty_tpl->tpl_vars['type']->value['typename'];?>

					</option>
					<?php } ?>
					<?php }?>
				</select>
				书名:
				<input type="text" id="title" name="title" placeholder="可输入书名精确查询名" />
				<span class="searchBtn">搜索</span>
			</div>
			<?php if ($_smarty_tpl->tpl_vars['bookList']->value) {?>
			<center>
				<form id="listForm">
					<table id="bookList" cellpadding="5" cellspacing="0">
						<tr>
							<th>图书号</th>
							<th>图书名</th>
							<th>作者</th>
							<th>出版时间</th>
							<th>简介</th>
							<th>操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['book'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['book']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bookList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['book']->key => $_smarty_tpl->tpl_vars['book']->value) {
$_smarty_tpl->tpl_vars['book']->_loop = true;
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>

							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['title'];?>

							</td>
							<td><?php echo $_smarty_tpl->tpl_vars['book']->value['author'];?>
</td>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['book']->value['pubtime'],"%Y-%m-%d");?>
</td>
							<td class="description">
								<?php if ($_smarty_tpl->tpl_vars['book']->value['description']) {?>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['description'];?>

								<?php } else { ?>暂无
								<?php }?>
							</td>
							<td>
								<?php if ($_smarty_tpl->tpl_vars['book']->value['is_lend']) {?>
								<span>已借出<span>
								<?php } else { ?>
								<span class="yudingBtn" name="<?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>
">预定</span>
								<?php }?>
							</td>
						</tr>
						<?php } ?>
					</table>
					<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['userInfo']->value['userName'];?>
" id="userName" />
				</form>
				<!--翻页条-->
				<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
				<div class="pageDiv"><?php echo $_smarty_tpl->tpl_vars['pageBox']->value;?>
</div>
				<?php }?>
			</center>
			<?php } else { ?>
			<?php if ($_smarty_tpl->tpl_vars['show_pageBox']->value) {?>
			<div class="noContent">暂时没有藏书</div>
			<?php }?>
			<?php }?>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
