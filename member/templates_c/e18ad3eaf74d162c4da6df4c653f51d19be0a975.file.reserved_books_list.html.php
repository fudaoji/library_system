<?php /* Smarty version Smarty-3.1.15, created on 2013-10-15 17:36:35
         compiled from "E:\www\library_system\templates\member\reserved_books_list.html" */ ?>
<?php /*%%SmartyHeaderCode:12804525bd04b7d8105-02153974%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e18ad3eaf74d162c4da6df4c653f51d19be0a975' => 
    array (
      0 => 'E:\\www\\library_system\\templates\\member\\reserved_books_list.html',
      1 => 1381829769,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12804525bd04b7d8105-02153974',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_525bd04b8521a2_28568864',
  'variables' => 
  array (
    'bookList' => 0,
    'book' => 0,
    'userInfo' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_525bd04b8521a2_28568864')) {function content_525bd04b8521a2_28568864($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'E:\\www\\library_system\\library\\smarty\\libs\\plugins\\modifier.date_format.php';
?>
<?php echo $_smarty_tpl->getSubTemplate ("../header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"我的图书预订单"), 0);?>


	<div id="main">
		<div id="main_title">您当前处于 
			<span class="weightFont">管理我的图书</span> >> 
			<span class="softFont">已预订书籍</span>
		</div>
		<div id="main_body">
			<?php if ($_smarty_tpl->tpl_vars['bookList']->value) {?>
			<center>
				<form id="listForm">
					<table id="bookList" cellpadding="5" cellspacing="0">
						<tr>
							<th>图书号</th>
							<th>图书名</th>
							<th>作者</th>
							<th>出版时间</th>
							<th>简介</th>
							<th>操作</th>
						</tr>
						<?php  $_smarty_tpl->tpl_vars['book'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['book']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bookList']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['book']->key => $_smarty_tpl->tpl_vars['book']->value) {
$_smarty_tpl->tpl_vars['book']->_loop = true;
?>
						<tr>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>

							</td>
							<td>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['title'];?>

							</td>
							<td><?php echo $_smarty_tpl->tpl_vars['book']->value['author'];?>
</td>
							<td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['book']->value['pubtime'],"%Y-%m-%d");?>
</td>
							<td class="description">
								<?php if ($_smarty_tpl->tpl_vars['book']->value['description']) {?>
								<?php echo $_smarty_tpl->tpl_vars['book']->value['description'];?>

								<?php } else { ?>暂无
								<?php }?>
							</td>
							<td>
								<span class="tuidingBtn" name="<?php echo $_smarty_tpl->tpl_vars['book']->value['bid'];?>
">退订</span>
							</td>
						</tr>
						<?php } ?>
					</table>
					<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['userInfo']->value['userName'];?>
" id="userName" />
				</form>
			</center>
			<?php } else { ?>
			<div class="noContent">您还未预订任何书籍</div>
			<?php }?>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("../footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
