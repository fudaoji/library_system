<?php 
	/**
	 * @desc 书籍分类列表页
	 */

	//公共部分
	include_once('./member_global.php');
	
	$userInfo = $base->auth();


	$cssArr = array('common.css', 'sixin_list.css');
	$jsArr = array('');
	$show_pageBox = true;

	$filter = array('table'=>$sixinTable,
					'key'=>'id',
					'where'=>"receiver='".$userInfo['userName']."'",
					);
	$sixinNum = $db->getNums($filter);
	$sixinList = getList(array('table'=>$sixinTable,'where'=>"receiver='".$userInfo['userName']."'", 'orderby'=>"sendtime DESC", 'limit'=>'0,'.EACHPAGE), $db);

	//翻页
	$totalPage = ceil($sixinNum/EACHPAGE);
	$currentPage = 1;
	if (isset($_GET['page']) && intval($_GET['page'])>=1) {
		$currentPage = $_GET['page'];
		$option = array("table"=>$sixinTable,'where'=>"receiver='".$userInfo['userName']."'", 'orderby'=>"sendtime DESC", "limit"=>($currentPage-1)*EACHPAGE.','.EACHPAGE);
		$sixinList = getList($option, $db);
	}
	$url = SITE_URL."member/sixin_list.php?page=";
	$pageBox = pageBox($totalPage,$currentPage, $url);

	
	$assignVar = array( "show_pageBox"=>$show_pageBox,
						"sixinList"=>$sixinList,
						"userInfo"=>$userInfo, 
						"cssArr"=>$cssArr, 
						"jsArr"=>$jsArr,
						"pageBox"=>$pageBox,
						);

	$smarty->assign($assignVar);

	$smarty->display(MEMBER_TEM_DIR."sixin_list.html");
?>
