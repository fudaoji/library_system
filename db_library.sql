-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 10 月 17 日 14:48
-- 服务器版本: 5.5.34
-- PHP 版本: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `db_library`
--

-- --------------------------------------------------------

--
-- 表的结构 `book_detail`
--

CREATE TABLE IF NOT EXISTS `book_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` varchar(30) NOT NULL COMMENT '图书号',
  `tid` varchar(30) NOT NULL COMMENT '类别ID',
  `title` varchar(50) NOT NULL COMMENT '书名',
  `description` text COMMENT '简介',
  `author` varchar(50) NOT NULL COMMENT '作者',
  `pubtime` int(11) NOT NULL COMMENT '出版日期',
  `is_show` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否可借',
  `is_lend` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否借出',
  PRIMARY KEY (`id`),
  UNIQUE KEY `bid` (`bid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书籍具体信息表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `book_type`
--

CREATE TABLE IF NOT EXISTS `book_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` varchar(30) NOT NULL COMMENT '类别号',
  `typename` varchar(50) NOT NULL COMMENT '类别名',
  `booknums` int(11) NOT NULL DEFAULT '0' COMMENT '此类图书数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书籍分类表' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `member_book`
--

CREATE TABLE IF NOT EXISTS `member_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` varchar(30) NOT NULL COMMENT '被借图书号',
  `username` varchar(50) NOT NULL COMMENT '借书用户账户',
  `begin_time` int(11) NOT NULL COMMENT '出借时间',
  `end_time` int(11) NOT NULL COMMENT '到期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='书籍出借情况' AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `sixin`
--

CREATE TABLE IF NOT EXISTS `sixin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(30) NOT NULL COMMENT '发送者',
  `receiver` varchar(30) NOT NULL COMMENT '接收者',
  `sixin` text NOT NULL COMMENT '私信内容',
  `sendtime` int(11) NOT NULL COMMENT '发送时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='私信数据表' AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- 表的结构 `system_msg`
--

CREATE TABLE IF NOT EXISTS `system_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL COMMENT '消息名称',
  `content` text NOT NULL COMMENT '消息内容',
  `pubtime` int(11) NOT NULL COMMENT '发布时间',
  `is_show` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否前台显示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='系统消息' AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `mid` tinyint(2) NOT NULL DEFAULT '2' COMMENT '用户组ID',
  `username` varchar(30) CHARACTER SET latin1 NOT NULL COMMENT '用户名',
  `realname` varchar(30) CHARACTER SET latin1 DEFAULT NULL COMMENT '真实姓名',
  `salt` char(8) CHARACTER SET latin1 NOT NULL COMMENT '加密参数',
  `password` char(32) CHARACTER SET latin1 NOT NULL COMMENT '用户密码',
  `sex` tinyint(2) NOT NULL DEFAULT '1' COMMENT '性别',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(30) DEFAULT NULL COMMENT '电子邮箱',
  PRIMARY KEY (`uid`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='用户表' AUTO_INCREMENT=31 ;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`uid`, `mid`, `username`, `realname`, `salt`, `password`, `sex`, `phone`, `email`) VALUES
(1, 1, 'admin', 'fudaoji', '2391', '3d26af1daff1a769141b040ff39075b0', 1, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
